mocha.setup('bdd');

const assert = chai.assert;

describe('changeSign', function() {
    it('should return ["-55"] if an argument is ["55"]', function() {
        const arr = ['55'];

        const actual = changeSign(arr);

        const expected = ['-55'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["44"] if an argument is ["-44"]', function() {
        const arr = ['-44'];

        const actual = changeSign(arr);

        const expected = ['44'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["44", "/", "-55"] if an argument is ["44", "/", "-55"]', function() {
        const arr = ['44', '/', '55'];

        const actual = changeSign(arr);

        const expected = ['44', '/', '-55'];
        assert.deepEqual(actual, expected);
    })

    it('should return [] if an agrument is []', function() {
        const arr = [];

        const actual = changeSign(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an agrument is invalid', function() {
        const arr = undefined;

        const actual = changeSign(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });
})

describe('backspace', function() {
    it('should return [] if an agrument is [1]', function() {
        const arr = [1];

        const actual = backspace(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return ["2"] if an agrument is ["22"]', function() {
        const arr = ['22'];

        const actual = backspace(arr);

        const expected = ['2'];
        assert.deepEqual(actual, expected);
    });

    it('should return [3] if an agrument is [33]', function() {
        const arr = [33];

        const actual = backspace(arr);

        const expected = ['3'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["4"] if an agrument is ["4", "+"]', function() {
        const arr = ['4', '+'];

        const actual = backspace(arr);

        const expected = ['4'];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an agrument is []', function() {
        const arr = [];

        const actual = backspace(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an agrument is invalid', function() {
        const arr = undefined;

        const actual = backspace(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });
});

describe('updateDisplay', function() {
    it('should return "111" if an argument is ["111"]', function() {
        const arr = ['111'];

        const actual = updateDisplay(arr);

        const expected = '111';
        assert.deepEqual(actual, expected);
    });

    it('should return "111+222" if an argument is ["111", "+", "222"]', function() {
        const arr = ['111', '+', '222'];

        const actual = updateDisplay(arr);

        const expected = '111+222';
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is invalid', function() {
        const arr = [];

        const actual = updateDisplay(arr);

        const expected = '';
        assert.deepEqual(actual, expected);
    });

    it('should return an apmpty sting if an argument is invalid', function() {
        const arr = undefined;

        const actual = updateDisplay(arr);

        const expected = '';
        assert.deepEqual(actual, expected);
    });
});

describe('updateOutput', function () {    
    it('should return ["5"] if arguments are ([], "5")', function() {
        const arg1 = [],
            arg2 = '5';

        const actual = updateOutput(arg1, arg2);

        const expected = ['5'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51"] if arguments are (["5"], "1")', function() {
        const arg1 = ['5'],
            arg2 = '1';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*"] if arguments are (["51"], "*")', function() {
        const arg1 = ['51'],
            arg2 = '*';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*", "6"] if arguments are (["51", "*"], "6")', function() {
        const arg1 = ['51', '*'],
            arg2 = '6';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*', '6'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*", "6", "+"] if arguments are (["51","*", "6", "+"], "+")', function() {
        const arg1 = ['51', '*', '6', '+'],
            arg2 = '+';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*', '6', '+'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*", "6."] if arguments are (["51","*", "6"], ".")', function() {
        const arg1 = ['51', '*', '6'],
            arg2 = '.';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*', '6.'];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if the first argument is invalid', function() {
        const arg1 = undefined,
            arg2 = '';

        const actual = updateOutput(arg1, arg2);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return incoming array if the last argument is invalid', function() {
        const arg1 = ['2','3'],
            arg2 = undefined;

        const actual = updateOutput(arg1, arg2);

        const expected = ['2','3'];
        assert.deepEqual(actual, expected);
    });
});

describe('calculateData', function() {
    it('should return ["4"] if an argument is ["2", "*", "2"]', function() {
        const arr = ['2', '*', '2'];

        const actual = calculateData(arr);

        const expected = [2 * 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["8"] if an argument is ["2", "*", "2", "*", "2"]', function() {
        const arr = ['2', '*', '2', '*', '2'];

        const actual = calculateData(arr);

        const expected = [2 * 2 * 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["2"] if an argument is ["2", "/", "2", "*", "2"]', function() {
        const arr = ['2', '/', '2', '*', '2'];

        const actual = calculateData(arr);

        const expected = [2 / 2 * 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["3"] if an argument is ["2", "/", "2", "+", "2"]', function() {
        const arr = ['2', '/', '2', '+', '2'];

        const actual = calculateData(arr);

        const expected = [2 / 2 + 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["-13"] if an argument is ["-2", "*", "-33", "/", "5.5", "+", "-1"]', function() {
        const arr = ['2', '*', '-33', '/', '5.5', '+', '-1'];

        const actual = calculateData(arr);

        const expected = [2 * -33 / 5.5 + -1  + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const arr = [];

        const actual = calculateData(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return ["Error!"] if an argument is invalid', function() {
        const arr = undefined;

        const actual = calculateData(arr);

        const expected = ["Error!"];
        assert.deepEqual(actual, expected);
    });
})

describe('filterArray', function() {
    it('should return ["1"] if an argument is [null, null, null, 1]', function() {
        const arr = [null, null, null, 1];

        const actual = filterArray(arr);

        const expected = ['1'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["1", "+", "2"] if an argument is [null, null, null, 1, "+", "2"]', function() {
        const arr = [null, null, null, 1, '+', '2'];

        const actual = filterArray(arr);

        const expected = ['1', '+', '2'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["2", "-", "1", "+", "2"] if an argument is ["2", "+", null, null, null, 1, "+", "2"]', function() {
        const arr = ['2', '-', null, null, null, 1, '+', '2'];

        const actual = filterArray(arr);

        const expected = ['2', '-', '1', '+', '2'];
        assert.deepEqual(actual, expected);
    });
    
    it('should return [] if an argument is []', function() {
        const arr = [];

        const actual = filterArray(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return ["Error!"] if an argument is [NaN, null, null, 1]', function() {
        const arr = [NaN, null, null, 1];

        const actual = filterArray(arr);

        const expected = ['Error!'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["Error!"] if an argument is invalid', function() {
        const arr = undefined;

        const actual = filterArray(arr);

        const expected = ['Error!'];
        assert.deepEqual(actual, expected);
    });
})

mocha.run();