const canvas = document.getElementById('canvas'),
      ctx = canvas.getContext('2d'),
      width = canvas.width,
      height = canvas.height,
      whiteColor = 'rgb(255,255,255)';

let balls = [];

canvas.addEventListener('click', onClick);
canvas.addEventListener('contextmenu', onClick);

class Ball {
  constructor(x, y, radius) {
    this.x = x ? x : getRandomNumber(0, width);
    this.y = y ? y : getRandomNumber(0, height);
    this.velocityX = getRandomNumber(-2, 2);
    this.velocityY = getRandomNumber(-2, 2);
    this.color = `rgb(${getRandomNumber(0, 255)}, 
                 ${getRandomNumber(0, 255)}, 
                 ${getRandomNumber(0, 255)})`;
    this.radius = radius ? radius : getRandomNumber(20, 50);
  }
  
  draw() {
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
    ctx.fill();
    ctx.fillStyle = whiteColor;
    ctx.stroke();
  }  
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

function updateCoords(ball) {
  if (ball.x + ball.velocityX < ball.radius || ball.x + ball.velocityX > width - ball.radius) {
    ball.velocityX = -ball.velocityX;
  }

  if (ball.y + ball.velocityY > height - ball.radius || ball.y + ball.velocityY < ball.radius) {
    ball.velocityY = -ball.velocityY;
  }

  ball.x += ball.velocityX;
  ball.y += ball.velocityY;
}

function isCollide(ball1, ball2) {
  const ballsDistance = Math.pow(ball1.x - ball2.x, 2) + Math.pow(ball1.y - ball2.y, 2),
        minBallDistance = Math.pow(ball1.radius + ball2.radius, 2);

  return ballsDistance <= minBallDistance;
}

function onClick(event) {
  const hitBall = getHitBall(event);

  switch (event.type) {
    case 'click' :
      if (hitBall) {
        splitBall(hitBall);
      } else {
        addBall(event.clientX, event.clientY);
      }   
      break;
    case 'contextmenu' :
      event.preventDefault();
      if (hitBall) {
        deleteBall(hitBall);
      }
      break;
  }
}

function getHitBall(event) {
  let targetBall = null;

  for (const ball of balls) {
    const distance = Math.sqrt(Math.pow(event.x - ball.x, 2) + Math.pow(event.y - ball.y, 2));
    
    if (distance <= ball.radius) targetBall = ball;
  };

  return targetBall;
}

function splitBall(ball) {
  const miniBallRadius = ball.radius / 8 + 3;

  deleteBall(ball);

  for (let i = 0; i < 8; i++) {
    addBall(event.clientX, event.clientY, miniBallRadius);    
  }
}


function addBall(x, y, radius) {
  balls.push(new Ball(x, y, radius));
}

function deleteBall(targetBall) {
  balls = balls.filter(ball => ball !== targetBall);
}

function absorbBall(ballOne, ballTwo) {
  if (ballOne.radius > ballTwo.radius) {
    ballOne.radius += ballTwo.radius;
    ballOne.velocityX *= 0.5;
    ballOne.velocityY *= 0.5;
    deleteBall(ballTwo);    
  }
}

function render() {
  for (const ballOne of balls) {
    ballOne.draw();
    updateCoords(ballOne);

    for (const ballTwo of balls) {
      if (ballOne !== ballTwo) {
        if (isCollide(ballOne, ballTwo)) {
          absorbBall(ballOne, ballTwo);
        }
      }
    }    
  }
}

function init() {
  ctx.fillRect(0, 0, width, height);
  ctx.fillStyle = whiteColor;

  render();
  
  requestAnimationFrame(init);
}

init();
