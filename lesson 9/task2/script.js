const canvas = document.getElementById('canvas'),
      ctx = canvas.getContext('2d'),
      width = canvas.width,
      height = canvas.height,
      whiteColor = 'rgb(255,255,255)';

let balls = localStorage.length ? JSON.parse(localStorage.getItem("balls")) : [];

canvas.addEventListener('click', onClick);

class Ball {
  constructor(x, y) {
    this.x = x ? x : getRandomNumber(0, width);
    this.y = y ? y : getRandomNumber(0, height);
    this.velocityX = getRandomNumber(-5, 5);
    this.velocityY = getRandomNumber(-5, 5);
    this.color = `rgb(${getRandomNumber(0, 255)}, 
                 ${getRandomNumber(0, 255)}, 
                 ${getRandomNumber(0, 255)})`;
    this.radius = getRandomNumber(20, 50);
  }
}

function draw(ball) {
  ctx.beginPath();
  ctx.fillStyle = ball.color;
  ctx.arc(ball.x, ball.y, ball.radius, 0, 2*Math.PI);
  ctx.fill();
  ctx.fillStyle = whiteColor;
  ctx.stroke();
}  

function pushOffBalls(ball1, ball2) {
  if (isCollide(ball1, ball2)) {
    changeBallsVectors(ball1, ball2);
  }
}

function isCollide(ball1, ball2) {
  const ballsDistance = Math.pow(ball1.x - ball2.x, 2) + Math.pow(ball1.y - ball2.y, 2),
        minBallDistance = Math.pow(ball1.radius + ball2.radius, 2);

  return ballsDistance <= minBallDistance;
}

function changeBallsVectors(ball1, ball2) {
  const direction = {
          x: ball1.x - ball2.x,
          y: ball1.y - ball2.y
        },
        velocity = {
          x: ball2.velocityX - ball1.velocityX,
          y: ball2.velocityY - ball1.velocityY,
        },
        dotProduct = velocity.x * direction.x + velocity.y * direction.y,
        mass = ball1.radius + ball2.radius,
        ballsDistance = Math.pow(direction.x, 2) + Math.pow(direction.y, 2),
        momentun = dotProduct / ballsDistance,
        scalar1 = (2 * ball2.radius / mass) * momentun,
        scalar2 = (2 * ball1.radius / mass) * momentun;

  if (dotProduct <= 0) {
  return;
  }

  ball1.velocityX = ball1.velocityX + (scalar1 * direction.x);
  ball1.velocityY = ball1.velocityY + (scalar1 * direction.y);
  ball2.velocityX = ball2.velocityX - (scalar2 * direction.x);
  ball2.velocityY = ball2.velocityY - (scalar2 * direction.y);
}

function updateCoords(ball) {
  if (ball.x + ball.velocityX < ball.radius || ball.x + ball.velocityX > width - ball.radius) {
    ball.velocityX = -ball.velocityX;
  }

  if (ball.y + ball.velocityY > height - ball.radius || ball.y + ball.velocityY < ball.radius) {
    ball.velocityY = -ball.velocityY;
  }

  ball.x += ball.velocityX;
  ball.y += ball.velocityY;
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

function init() {
  ctx.fillRect(0, 0, width, height);
  ctx.fillStyle = whiteColor;

  for (const ballOne of balls) {
    draw(ballOne);
    updateCoords(ballOne);

    for (const ballTwo of balls) {
      if (ballOne !== ballTwo) {
        pushOffBalls(ballOne, ballTwo);
      }
    }
  }

  localStorage.setItem("balls", JSON.stringify(balls));

  requestAnimationFrame(init);
}

function onClick(event) {
  const hitBall = getHitBall(event);

  if (hitBall) {
    deleteBall(hitBall);
  } else {
    addBall(event.clientX, event.clientY);
  }
}

function deleteBall(targetBall) {
  balls = balls.filter(ball => ball !== targetBall);
}

function addBall(x, y) {
  balls.push(new Ball(x, y));
}

function getHitBall(event) {
  let targetBall = null;

  for (const ball of balls ) {
    const distance = Math.sqrt(Math.pow(event.x - ball.x, 2) + Math.pow(event.y - ball.y, 2));
    
    if (distance <= ball.radius) targetBall = ball;
  };

  return targetBall;
}

init();
