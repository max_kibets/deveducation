// mocha.setup('bdd');

// const assert = chai.assert;

// describe('draw', function() {
//   let sandbox = null;

// 	before(() => {
// 		sandbox = sinon.createSandbox();
// 	});

// 	afterEach(() => {
// 		sandbox.restore();
//   });

//   it('should call beginPath in draw', function() {
//     const ball = new Ball();    
//     const stub = sandbox.stub(ctx, 'beginPath');

//     ball.draw();

//     sandbox.assert.calledOnce(stub);
//   });

//   it('should call arc in draw', function() {
//     const ball = new Ball();    
//     const stub = sandbox.stub(ctx, 'arc');

//     ball.draw();

//     sandbox.assert.calledOnce(stub);
//   });

//   it('should call fill in draw', function() {
//     const ball = new Ball();    
//     const stub = sandbox.stub(ctx, 'fill');

//     ball.draw();

//     sandbox.assert.calledOnce(stub);
//   });

//   it('should call stroke in draw', function() {
//     const ball = new Ball();    
//     const stub = sandbox.stub(ctx, 'stroke');

//     ball.draw();

//     sandbox.assert.calledOnce(stub);
//   });  
// });

// describe('updateCoords', function() {
//   it('should update coords on velocity values', function() {
//     const ball = new Ball();
//     ball.x = 560;
//     ball.y = 770;
//     ball.velocityX = 5;
//     ball.velocityY = -4;

//     const actual = updateCoords(ball);

//     const expected = {
//       x: 565,
//       y: 774
//     };
//     assert.deepEqual(actual, expected);
//   });
// });

// describe('deleteBall', function() {
//   it('should delete ball from array of balls', function() {
//     const ball = {
//       x: 200,
//       y: 500,
//       velocityX: 2, 
//       velocityY: -2,
//       color: 'rgb(0, 0, 0)'
//     };
//     balls[0] = ball;

//     deleteBall(ball);
//     const actual = balls;

//     const expected = [];
//     assert.deepEqual(actual, expected);
//   })
// });

// // describe('onCanvasClick', function() {
// //   let sandbox;
  
// //     before(() => {
// //       sandbox = sinon.createSandbox();
// //     });
  
// //     afterEach(() => {
// //       sandbox.restore();
// //     });

// //     it('should call onCanvasClick function', function() {
// //       const stub = sandbox.stub(window, 'onCanvasClick');
      
// // 		  canvas.click();
  
// //       sandbox.assert.calledOnce(stub);      
// //     });  
// // });

// describe('init', function() {
//   let sandbox;
  
//     before(() => {
//       sandbox = sinon.createSandbox();
//     });
  
//     afterEach(() => {
//       sandbox.restore();
//     });

//     it('should call fillRect function', function() {
//       const stub = sandbox.stub(ctx, 'fillRect');
      
// 		  init();
  
//       sandbox.assert.calledOnce(stub);      
//     });  
// });

// mocha.run();