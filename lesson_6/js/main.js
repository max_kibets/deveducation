class Llist {
    root = null;

    createNode = (value, prev = null, next = null) => {
        return {
            value,
            prev,
            next
        }
    };

    _length = (node) => {
        if (!node) return 0;

        let nodesCount = 1;

        (function countItems(unit) {
            if (unit.next) {
                nodesCount++;
                countItems(unit.next);
            };
        })(node);

        return nodesCount;
    };

    _lastNode = (node) => {
        if (!node) return node;

        while (node.next) {
            node = node.next;
        };

        return node;
    };

    push = (...rest) => {
        for (const value of rest) {
            let lastNode = this._lastNode(this.root),
                node = this.createNode(value, lastNode);

            if (this.root) lastNode.next = node;
            else this.root = node;
        };

        return this._length(this.root);
    };

    pop = () => {
        let deletedNode = this.root ? this._lastNode(this.root) : {value: void 0};

        if (deletedNode.prev) {
            deletedNode.prev.next = null;
        } else {
            this.root = null;
        };

        return deletedNode.value;
    };

    unshift(...rest) {
        if (!rest.length) {
            return this._length(this.root);
        }

        let newFirstNode = null;
        let firstNode = null;

        for (const value of rest) {
            const node = this.createNode(value, firstNode);

            if (!firstNode) {
                firstNode = node;
                newFirstNode = node;
            } else {
                firstNode.next = node;
                firstNode = firstNode.next;
            }
        }

        if (this.root) this.root.prev = firstNode;

        firstNode.next = this.root;
        this.root = newFirstNode;
        this.length += rest.length;

        return this._length(this.root);
    }

    shift() {
        let deletedElement = void 0;

        if (!this.root) {
            return deletedElement;
        }

        deletedElement = this.root.value;
        this.root = this.root.next;

        if(this.root) this.root.prev = null;

        this.length = this.length - 1;

        return deletedElement;
    }
    
    toString = () => {
        if (!this.root) return '';
        let str = '';

        (function concatValues(node) {
            if (node.next) {
                str += `${node.value}, `;
                concatValues(node.next);
            } else {
                str += node.value;
            };
        })(this.root);

        return str;
    };

    map = (callback) => {
        if (typeof callback !== 'function' || !this.root) return void 0;

        let newNode = null,
            node = this.root;

        do {
            const value = callback(node.value);

            if (newNode) {
                const lastNode = this._lastNode(newNode);

                lastNode.next = this.createNode(value, lastNode);
            } else {
                newNode = this.createNode(value);
            };

            node = node.next;
        } while (node);

        return newNode;
    }
}

let llist = new Llist();
