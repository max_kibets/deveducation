mocha.setup('bdd');

const assert = chai.assert;

// LINKED LIST
describe('Llist', function() {
    describe('createNode', function() {
        it('should return {value: undefined, prev: null, next: null} if an argument is empty', function() {
            const llist = new Llist();
    
            const actual = llist.createNode();
    
            const expected = {value: undefined, prev: null, next: null};
            assert.deepEqual(actual, expected);
        });
    
        it('should return {value: 1, prev: null, next: null} if an argument is 1', function() {
            const data = 1;
                llist = new Llist();
    
            const actual = llist.createNode(data);
    
            const expected = {value: 1, prev: null, next: null};
            assert.deepEqual(actual, expected);
        });
    });

    describe('_length', function() {
        it('should return 0 if an argument is null', function() {
            const data = null,
                llist = new Llist();

            const actual = llist._length(data);

            const expected = 0;
            assert.equal(actual, expected);
        });

        it('should return 1 if an argument is {value: 1, next: null}', function() {
            const data = {value: 1, next: null},
                llist = new Llist();

            const actual = llist._length(data);

            const expected = 1;
            assert.equal(actual, expected);
        });

        it('should return 3 if an argument is {value: 1, next: {value: 2, next: {value: 3, next: null}}}', function() {
            const data = {value: 1, next: {value: 2, next: {value: 3, next: null}}},
                llist = new Llist();

            const actual = llist._length(data);

            const expected = 3;
            assert.equal(actual, expected);
        });
    });

    describe('push', function() {
        it('should return 0 if an argument is empty and Llist.root is empty', function() {
            const llist = new Llist();
            
            const actual = llist.push();

            const expected = 0;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if an argument is empty and Llist.root is empty', function() {
            const llist = new Llist();
            
            llist.push();
            const actual = llist.root;

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 1 if an argument is 11', function() {
            const data = 11, 
                llist = new Llist();
                
            
            const actual = llist.push(data);

            const expected = 1;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 1 node if an argument is 11 and Llist.root is empty', function() {
            const data = 11,
                llist = new Llist();
            
            llist.push(data);
            const actual = llist.root;

            const expected = {value: 11, prev: null, next: null};
            assert.deepEqual(actual, expected);
        });

        it('should return 2 if argument are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            const actual = llist.push(data1, data2);

            const expected = 2;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 2 nodes if arguments are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            llist.push(data1, data2);
            const actual = llist.root;

            const firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: null, next: null},
                expected = firstNode;
            firstNode.next = secondNode;
            secondNode.prev = firstNode;
            assert.deepEqual(actual, expected);
        });

        it('should return 2 if argument are (22) and Llist.root contains 1 node', function() {
            const data = 22,
                llist = new Llist();
            llist.root = {value: 11, prev: null, next: null};

            const actual = llist.push(data);

            const expected = 2;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 2 node if arguments are (22) and Llist.root contains 1 node', function() {
            const data = 22,
                llist = new Llist();
            llist.root = {value: 11, prev: null, next: null};
            
            llist.push(data);
            const actual = llist.root;

            const firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: null, next: null},
                expected = firstNode;
            firstNode.next = secondNode;
            secondNode.prev = firstNode;
            assert.deepEqual(actual, expected);
        });
    });

    describe('pop', function() {
        it('should return undefined if Llist.root was empty', function() {
            const llist = new Llist();
    
            const actual = llist.pop();
    
            const expected = undefined;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root was empty', function() {
            const llist = new Llist();
    
            llist.pop();
            const actual = llist.root;
    
            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 11 if Llist.root contained 1 node with value: 11', function() {
            const llist = new Llist();
            llist.root = {value: 11, prev: null, next: null};
    
            const actual = llist.pop();
    
            const expected = 11;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root contained 1 node', function() {
            const llist = new Llist();
            llist.root = {value: 11, prev: null, next: null};
    
            llist.pop();
            const actual = llist.root;
    
            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 33 if List.root contained 3 nodes, the last node with value: 33', function() {
            const llist = new Llist(),
                firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: firstNode, next: null},
                thirdNode = {value: 33, prev: secondNode, next: null};
            firstNode.next = secondNode;
            secondNode.next = thirdNode;
            llist.root = firstNode;
    
            const actual = llist.pop();
    
            const expected = 33;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 2 nodes if Llist.root contained 3 nodes', function() {
            const llist = new Llist(),
                firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: firstNode, next: null},
                thirdNode = {value: 33, prev: secondNode, next: null};
            firstNode.next = secondNode;
            secondNode.next = thirdNode;
            llist.root = firstNode;
            
            llist.pop();
            const actual = llist.root;

            const expFirstNode = {value: 11, prev: null, next: null},
                expSecondNode = {value: 22, prev: expFirstNode, next: null},
                expected = expFirstNode;
            expFirstNode.next = expSecondNode;
            assert.deepEqual(actual, expected);
        });
    });

    describe('unshift', function() {
        it('should return 0 if an argument is empty and Llist.root was empty', function() {
            const llist = new Llist();
            
            const actual = llist.unshift();

            const expected = 0;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if an argument is empty and Llist.root was empty', function() {
            const llist = new Llist();
            
            llist.unshift();
            const actual = llist.root;

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 1 if an argument is 11 and Llist.root was empty', function() {
            const data = 11, 
                llist = new Llist();
            
            const actual = llist.unshift(data);

            const expected = 1;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 1 node if an argument is 11 and Llist.root was empty', function() {
            const data = 11,
                llist = new Llist();
            
            llist.unshift(data);
            const actual = llist.root;

            const expected = {value: 11, prev: null, next: null};
            assert.deepEqual(actual, expected);
        });

        it('should return 2 if arguments are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            const actual = llist.unshift(data1, data2);

            const expected = 2;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 2 node if arguments are (11, 22) and Llist.root was empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            llist.unshift(data1, data2);
            const actual = llist.root;

            const expFirstNode = {value: 11, prev: null, next: null},
                expSecondNode = {value: 22, prev: expFirstNode, next: null},
                expected = expFirstNode;
            expFirstNode.next = expSecondNode;
            assert.deepEqual(actual, expected);
        });

        it('should return 3 if arguments are (11, 22) and Llist.root contained 1 node', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            llist.root = {value: 33, prev: null, next: null};
            
            const actual = llist.unshift(data1, data2);

            const expected = 3;
            assert.equal(actual, expected);
        });

        it('Llist.root should contains 3 nodes if arguments are (11, 22) and Llist.root contained 1 node', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            llist.root = {value: 33, prev: null, next: null};
            
            llist.unshift(data1, data2);
            const actual = llist.root;

            const expFirstNode = {value: 11, prev: null, next: null},
                expSecondNode = {value: 22, prev: expFirstNode, next: null},
                expThirdNode = {value: 33, prev: expSecondNode, next: null},
                expected = expFirstNode;
            expFirstNode.next = expSecondNode;
            expSecondNode.next = expThirdNode;
            assert.deepEqual(actual, expected);
        });
    });

    describe('shift', function() {
        it('should return undefined if Llist.root was empty', function() {
            const llist = new Llist();

            const actual = llist.shift();

            const expected = undefined;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root was empty', function() {
            const llist = new Llist();

            llist.shift();
            const actual = llist.root;

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 11 if Llist.root contained 1 node', function() {
            const llist = new Llist();
            llist.root = {value: 11, prev: null, next: null};
    
            const actual = llist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root contained 1 node', function() {
            const llist = new Llist();
            llist.root = {value: 11, prev: null, next: null};
    
            llist.shift();
            const actual = llist.root;
    
            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 11 if List.root contained 3 nodes and the first node value was 11', function() {
            const llist = new Llist(),
                firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: firstNode, next: null},
                thirdNode = {value: 33, prev: secondNode, next: null};
            firstNode.next = secondNode;
            secondNode.next = thirdNode;
            llist.root = firstNode;
    
            const actual = llist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);          
        });

        it('Llist.root should contain 2 nodes if Llist.root contained 3 nodes', function() {
            const llist = new Llist(),
                firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: firstNode, next: null},
                thirdNode = {value: 33, prev: secondNode, next: null};
            firstNode.next = secondNode;
            secondNode.next = thirdNode;
            llist.root = firstNode;
            
            llist.shift();
            const actual = llist.root;

            const expFirstNode = {value: 22, prev: null, next: null},
                expSecondNode = {value: 33, prev: expFirstNode, next: null},
                expected = expFirstNode;
            expFirstNode.next = expSecondNode;
            assert.deepEqual(actual, expected);
        });
    });

    describe('toString', function() {
        it('should return an empty string if Llist.root is epmty', function() {
            const llist = new Llist();
    
            const actual = llist.toString();
    
            const expected = '';
            assert.equal(actual, expected);
        });
    
        it('should return "11" if Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: null};
    
            const actual = llist.toString();
    
            const expected = '11';
            assert.equal(actual, expected);
        });
    
        it('should return "11, 22, 33" if Llist.root is {value: 11, next: {value: 22, next: {value: 33, next: null}}}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
    
            const actual = llist.toString();
    
            const expected = '11, 22, 33';
            assert.equal(actual, expected);
        });
    });

    describe('map', function() {
        it('should return undefined if an argument is not a function', function() {
            const llist = new Llist()
                data = 'invalid data';

            const actual = llist.map(data);

            const expected = undefined;
            assert.equal(actual, expected);
        });

        it('should return null if an argument is ((item) => item * 2) and Llist.root is null', function() {
            const llist = new Llist(),
                data = (item) => item * 2;
            
            const actual = llist.map(data);

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 1 node with value 22 if an argument is ((item) => item * 2) and Llist.root contains 1 node', function() {
            const llist = new Llist(),
                data = (item) => item * 2;
            llist.root = {value: 11, prev: null, next: null};
            
            const actual = llist.map(data);

            const expected = {value: 22, prev: null, next: null};
            assert.deepEqual(actual, expected);
        });

        it('should return 2 nodes with values 121 and 484 if an argument is ((item) => item * item) and Llist.root contained 2 nodes with values 11 and 22', function() {
            const llist = new Llist(),
                data = (item) => item * item,
                firstNode = {value: 11, prev: null, next: null},
                secondNode = {value: 22, prev: firstNode, next: null};
            firstNode.next = secondNode;
            llist.root = firstNode;
            
            const actual = llist.map(data);

            const expFirstNode = {value: 121, prev: null, next: null},
                expSecondNode = {value: 484, prev: expFirstNode, next: null},
                expected = expFirstNode;
            expFirstNode.next = expSecondNode;
            assert.deepEqual(actual, expected);
        });
    });
});

mocha.run();