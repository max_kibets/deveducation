mocha.setup('bdd');

const assert = chai.assert;

describe('findMax', function() {
    it('should return 1 if arguments are (-3, 1)', function() {
        const a = -3,
            b = 1;

        const actual = findMax(a, b);

        const expected = 1;
        assert.equal(actual, expected);
    });

    it('should return 3 if arguments are (3, 1)', function() {
        const a = 3,
            b = 1;

        const actual = findMax(a, b);

        const expected = 3;
        assert.equal(actual, expected);
    });

    it('should return 0 if arguments are (0, 0)', function() {
        const a = 0,
            b = 0;

        const actual = findMax(a, b);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the second argument is invalid', function() {
        const a = 2,
            b = undefined;

        const actual = findMax(a, b);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the first argument is invalid', function() {
        const a = undefined,
            b = 2;

        const actual = findMax(a, b);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getGrade', function() {
    it('should return "A" if an argument is 90', function() {
        const data = 90;

        const actual = getGrade(data);

        const expected = 'A';
        assert.equal(actual, expected);
    });

    it('should return "B" if an argument is 75', function() {
        const data = 75;

        const actual = getGrade(data);

        const expected = 'B';
        assert.equal(actual, expected);
    });

    it('should return "C" if an argument is 60', function() {
        const data = 60;

        const actual = getGrade(data);

        const expected = 'C';
        assert.equal(actual, expected);
    });

    it('should return "D" if an argument is 40', function() {
        const data = 40;

        const actual = getGrade(data);

        const expected = 'D';
        assert.equal(actual, expected);
    });

    it('should return "E" if an argument is 20', function() {
        const data = 20;

        const actual = getGrade(data);

        const expected = 'E';
        assert.equal(actual, expected);
    });

    it('should return "F" if an argument is 0', function() {
        const data = 0;

        const actual = getGrade(data);

        const expected = 'F';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is -1', function() {
        const data = -1;

        const actual = getGrade(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is 101', function() {
        const data = 101;

        const actual = getGrade(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getGrade(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getEvenSum', function() {
    it('should return {sum: 2450, count: 49} if arguments are (1, 99)', function() {
        const start = 1,
            end = 99;

        const actual = getEvenSum(start, end);

        const expected = {sum: 2450, count: 49};
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if arguments are (90, 10)', function() {
        const start = 0,
            end = 99;

        const actual = getEvenSum(start, end);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the first argument is 0', function() {
        const start = 0,
            end = 99;

        const actual = getEvenSum(start, end);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the second argument is 0', function() {
        const start = 1,
            end = 0;

        const actual = getEvenSum(start, end);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the first argument is invalid', function() {
        const start = undefined,
            end = 99;

        const actual = getEvenSum(start, end);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the second argument is invalid', function() {
        const start = 1,
            end = undefined;

        const actual = getEvenSum(start, end);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('isPrimeNumbers', function() {
    it('should return true if an argument is 2', function() {
        const data = 2;

        const actual = isPrimeNumbers(data);

        const expected = true;
        assert.equal(actual, expected);
    });

    it('should return false if an argument is 1', function() {
        const data = 1;

        const actual = isPrimeNumbers(data);

        const expected = false;
        assert.equal(actual, expected);
    });

    it('should return false if an argument is 0', function() {
        const data = 0;

        const actual = isPrimeNumbers(data);

        const expected = false;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is -1', function() {
        const data = -1;

        const actual = isPrimeNumbers(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = isPrimeNumbers(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getSqrt', function() {
    it('should return 11 if an argument is 121', function() {
        const data = 121;

        const actual = getSqrt(data);

        const expected = 11;
        assert.equal(actual, expected);
    });

    it('should return 1 if an argument is 1', function() {
        const data = 1;

        const actual = getSqrt(data);

        const expected = 1;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is -121', function() {
        const data = -121;

        const actual = getSqrt(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is 0', function() {
        const data = 0;

        const actual = getSqrt(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getSqrt(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getFuck', function() {
    it('should return 120 if an argument is 5', function() {
        const data = 5;

        const actual = getFuck(data);

        const expected = 120;
        assert.equal(actual, expected);
    });

    it('should return 1 if an argument is 1', function() {
        const data = 1;

        const actual = getFuck(data);

        const expected = 1;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is -1', function() {
        const data = -1;

        const actual = getFuck(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is 0', function() {
        const data = 0;

        const actual = getFuck(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getFuck(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getNumSum', function() {
    it('should return 4 if an argument is 22', function() {
        const data = 22;

        const actual = getNumSum(data);

        const expected = 4;
        assert.equal(actual, expected);
    });

    it('should return -4 if an argument is -22', function() {
        const data = -22;

        const actual = getNumSum(data);

        const expected = -4;
        assert.equal(actual, expected);
    });

    it('should return 2 if an argument is 2', function() {
        const data = 2;

        const actual = getNumSum(data);

        const expected = 2;
        assert.equal(actual, expected);
    });

    it('should return 0 if an argument is 0', function() {
        const data = 0;

        const actual = getNumSum(data);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getNumSum(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getMirrorNum', function() {
    it('should return 1234 if an argument is 4321', function() {
        const data = 4321;

        const actual = getMirrorNum(data);

        const expected = 1234;
        assert.equal(actual, expected);
    });    

    it('should return -1234 if an argument is -4321', function() {
        const data = -4321;

        const actual = getMirrorNum(data);

        const expected = -1234;
        assert.equal(actual, expected);
    });

    it('should return 1 if an argument is 1', function() {
        const data = 1;

        const actual = getMirrorNum(data);

        const expected = 1;
        assert.equal(actual, expected);
    });

    it('should return 0 if an argument is 0', function() {
        const data = 0;

        const actual = getMirrorNum(data);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getMirrorNum(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('findMinArr', function() {
    it('should return {min: 66, index: 2} if an argument is [1, 23, -66]', function() {
        const data = [1, 23, -66];

        const actual = findMinArr(data);

        const expected = {min: -66, index: 2};
        assert.deepEqual(actual, expected);
    });

    it('should return {min: 1, index: 0} if an argument is [1, 23, 66]', function() {
        const data = [1,23,66];

        const actual = findMinArr(data);

        const expected = {min: 1, index: 0};
        assert.deepEqual(actual, expected);
    });

    it('should return {min: 1, index: 0} if an argument is [1]', function() {
        const data = [1];

        const actual = findMinArr(data);

        const expected = {min: 1, index: 0};
        assert.deepEqual(actual, expected);
    });

    it('should return {min: 0, index: 0} if an argument is []', function() {
        const data = [];

        const actual = findMinArr(data);

        const expected = {min: 0, index: 0};
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = findMinArr(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('findMaxArr', function() {
    it('should return {max: 66, index: 2} if an argument is [1, 23, -66]', function() {
        const data = [1, 23, -66];

        const actual = findMaxArr(data);

        const expected = {max: 23, index: 1};
        assert.deepEqual(actual, expected);
    });

    it('should return {max: 66, index: 2} if an argument is [1, 23, 66]', function() {
        const data = [1,23,66];

        const actual = findMaxArr(data);

        const expected = {max: 66, index: 2};
        assert.deepEqual(actual, expected);
    });

    it('should return {max: 1, index: 0} if an argument is [1]', function() {
        const data = [1];

        const actual = findMaxArr(data);

        const expected = {max: 1, index: 0};
        assert.deepEqual(actual, expected);
    });

    it('should return {max: 0, index: 0} if an argument is []', function() {
        const data = [];

        const actual = findMaxArr(data);

        const expected = {max: 0, index: 0};
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = findMaxArr(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getOddIndexSum', function() {
    it('should return 6 if an argument is [1, 2, 3, 4]', function() {
        const data = [1, 2, 3, 4];

        const actual = getOddIndexSum(data);

        const expected = 6;
        assert.equal(actual, expected);
    });

    it('should return 1 if an argument is [1, 2]', function() {
        const data = [1, 2];

        const actual = getOddIndexSum(data);

        const expected = 2;
        assert.equal(actual, expected);
    });

    it('should return 0 if an argument is [1]', function() {
        const data = [1];

        const actual = getOddIndexSum(data);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return 0 if an argument is []', function() {
        const data = [];

        const actual = getOddIndexSum(data);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getOddIndexSum(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('reverseArr', function() {
    it('should return [1, 2, 3, 4] if an argument is [4, 3, 2, 1]', function() {
        const data = [4, 3, 2, 1];

        const actual = reverseArr(data);

        const expected = [1, 2, 3, 4];
        assert.deepEqual(actual, expected);
    });

    it('should return [1, 2, 3] if an argument is [3, 2, 1]', function() {
        const data = [3, 2, 1];

        const actual = reverseArr(data);

        const expected = [1, 2, 3];
        assert.deepEqual(actual, expected);
    });

    it('should return [1] if an argument is [1]', function() {
        const data = [1];

        const actual = reverseArr(data);

        const expected = [1];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const data = [];

        const actual = reverseArr(data);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = reverseArr(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getOddCount', function() {
    it('should return 4 if an argument is [5, 5, 2, 2, 22, 5, 8, 6, 2, 1]', function() {
        const data = [5, 5, 2, 2, 22, 5, 8, 6, 2, 1];

        const actual = getOddCount(data);

        const expected = 4;
        assert.equal(actual, expected);
    });

    it('should return 1 if an argument is [1, 2]', function() {
        const data = [1];

        const actual = getOddCount(data);

        const expected = 1;
        assert.equal(actual, expected);
    });

    it('should return 0 if an argument is [2]', function() {
        const data = [2];

        const actual = getOddCount(data);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return 0 if an argument is []', function() {
        const data = [];

        const actual = getOddCount(data);

        const expected = 0;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getOddCount(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('swapHalves', function() {
    it('should return [1, 4, ":", 8, 8] if an argument is [8, 8, ":", 1, 4]', function() {
        const data = [8, 8, ':', 1, 4];

        const actual = swapHalves(data);

        const expected = [1, 4, ':', 8, 8];
        assert.deepEqual(actual, expected);
    });

    it('should return [1, 4, 8, 8] if an argument is [8, 8, 1, 4]', function() {
        const data = [8, 8, 1, 4];

        const actual = swapHalves(data);

        const expected = [1,4,8,8];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const data = [];

        const actual = swapHalves(data);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = swapHalves(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
})

describe('sortBubble', function() {
    it('should return [-6,1,2,3,4,5] if an argument is [2,5,4,-6,1,3]', function() {
        const data = [2,5,4,-6,1,3];

        const actual = sortBubble(data);

        const expected = [-6,1,2,3,4,5];
        assert.deepEqual(actual, expected);
    });

    it('should return [1,2,3,4,5] if an argument is [2,5,4,1,3]', function() {
        const data = [2,5,4,1,3];

        const actual = sortBubble(data);

        const expected = [1,2,3,4,5];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const data = [];

        const actual = sortBubble(data);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = sortBubble(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('sortSelect', function() {
    it('should return [-6,1,2,3,4,5] if an argument is [2,5,4,-6,1,3]', function() {
        const data = [2,5,4,-6,1,3];

        const actual = sortSelect(data);

        const expected = [-6,1,2,3,4,5];
        assert.deepEqual(actual, expected);
    });

    it('should return [1,2,3,4,5] if an argument is [2,5,4,1,3]', function() {
        const data = [2,5,4,1,3];

        const actual = sortSelect(data);

        const expected = [1,2,3,4,5];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const data = [];

        const actual = sortSelect(data);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = sortSelect(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('sortInsert', function() {
    it('should return [-6,1,2,3,4,5] if an argument is [2,5,4,-6,1,3]', function() {
        const data = [2,5,4,-6,1,3];

        const actual = sortInsert(data);

        const expected = [-6,1,2,3,4,5];
        assert.deepEqual(actual, expected);
    });

    it('should return [1,2,3,4,5] if an argument is [2,5,4,1,3]', function() {
        const data = [2,5,4,1,3];

        const actual = sortInsert(data);

        const expected = [1,2,3,4,5];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const data = [];

        const actual = sortInsert(data);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = sortInsert(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

describe('getDay', function() {
    it('should return "Пн" if an argument is 1', function() {
        const data = 1;

        const actual = getDay(data);

        const expected = 'Пн';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is -8', function() {
        const data = -8;

        const actual = getDay(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is 8', function() {
        const data = 8;

        const actual = getDay(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is 0', function() {
        const data = 0;

        const actual = getDay(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getDay(data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
})

describe('getPower', function() {
    it('should return 30.25 if an argument is 5.5', function() {
        const data = 5.5;

        const actual = getPower(data);

        const expected = 30.25;
        assert.equal(actual, expected);
    });

    it('should return 25 if an argument is 5', function() {
        const data = 5;

        const actual = getPower(data);

        const expected = 25;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if an argument is invalid', function() {
        const data = undefined;

        const actual = getPower(data, 1, 1, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
})

describe('getLength', function() {
    it('should return 42.42640687119285 if the first argument is (10, 40, 10, 40)', function() {
        const x1 = 10,
            x2 = 40,
            x3 = 10,
            x4 = 40;

        const actual = getLength(x1, x2, x3, x4);

        const expected = 42.42640687119285;
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the first argument is -1', function() {
        const data = -1;

        const actual = getLength(data, 1, 1, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the second argument is -1', function() {
        const data = undefined;

        const actual = getLength(1, data, 1, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the third argument is -1', function() {
        const data = undefined;

        const actual = getLength(1, 1, data, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the fourth argument is -1', function() {
        const data = undefined;

        const actual = getLength(1, 1, 1, data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the first argument is invalid', function() {
        const data = undefined;

        const actual = getLength(data, 1, 1, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the second argument is invalid', function() {
        const data = undefined;

        const actual = getLength(1, data, 1, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the third argument is invalid', function() {
        const data = undefined;

        const actual = getLength(1, 1, data, 1);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });

    it('should return "invalid incoming data" if the fourth argument is invalid', function() {
        const data = undefined;

        const actual = getLength(1, 1, 1, data);

        const expected = 'invalid incoming data';
        assert.equal(actual, expected);
    });
});

mocha.run();