"use strict";

// Условные операторы:
//----------------------------------------------------------------------------------------
console.log('Условные операторы:');

// 1) Если а – четное посчитать а*б, иначе а+б:
function multiplyOrAdd(a, b) {
    if (a < 0 || b < 0) return 'Хули тут так мало?!';

    if (!isFinite(a) || !isFinite(b) || typeof a !== 'number' || typeof b !== 'number') {
        return 'invalid incoming data';
    }

    return a % 2 ? (a + b) : (a * b);
}
console.log('1) ', multiplyOrAdd(6, 2), multiplyOrAdd(5, 2));

// 2) Определить какой четверти принадлежит точка с координатами (х,у)
function findQuarter(x, y) {
    if (x === 0 || y === 0) return 'Не верно заданы координаты';

    if (!isFinite(x) || !isFinite(y) || typeof x !== 'number' || typeof y !== 'number') {
        return 'invalid incoming data';
    }

    if (x > 0 && y > 0) {
        return 'I';
    } else if (x < 0 && y > 0) {
        return 'II';
    } else if (x < 0 && y < 0) {
        return 'III';
    } else if (x > 0 && y < 0) {
        return 'IV';
    }
}
console.log('2) ', findQuarter(1, 1), findQuarter(-1, 1), findQuarter(-1, -1), findQuarter(1, -1));

// 3) Найти суммы только положительных из трех чисел
function getPositiveSum(a, b, c) {
    if (!isFinite(a) || !isFinite(b) || !isFinite(c) || typeof a !== 'number' || typeof b !== 'number' || typeof c !== 'number') {
        return "invalid incoming data";
    };

    let sum = 0;

    if (a > 0) sum += a;
    if (b > 0) sum += b;
    if (c > 0) sum += c;

    return sum;
}
console.log('3) ', getPositiveSum(1, 2, -3));

// 4) Посчитать выражение макс(а * б * с, а + б + с) + 3
function findMax(a, b) {
    if (typeof a !== 'number' || typeof b !== 'number') {
        return "invalid incoming data";
    };

    return a > b ? a : b;
}
console.log('4) ', findMax((2 * 2 * 2), (20 + 2 + 2)) + 3);

// 5) Написать программу определения оценки студента по его рейтингу, на основе следующих правил
function getGrade(num) {
    if ((!num && num !== 0) || (num < 0 || num > 100)) {
        return "invalid incoming data";
    };

    if (num >= 0 && num <= 19) {
        return 'F';
    } else if (num >= 20 && num <= 39) {
        return 'E';
    } else if (num >= 40 && num <= 59) {
        return 'D';
    } else if (num >= 60 && num <= 74) {
        return 'C';
    } else if (num >= 75 && num <= 89) {
        return 'B';
    } else if (num >= 90 && num <= 100) {
        return 'A';
    };
}
console.log('5) ', getGrade(86));

// Циклы
//----------------------------------------------------------------------------------------
console.log('Циклы:');

// 1) Найти сумму четных чисел и их количество в диапазоне от 1 до 99
function getEvenSum(start, end) {
    if ((!+start || (start < 0 || start > 99)) 
        || (!+end || (end < 0 || end > 99))
        || start > end) {
        return "invalid incoming data";
    };

    let sum = 0,
        count = 0;

    for (let i = start; i <= end; i++) {
        if (!(i % 2)) {
            sum += i;
            count++;
        }
    };

    return {
        sum,
        count
    };
}
console.log('1) ', getEvenSum(1, 99));

// 2) Проверить простое ли число ? (число называется простым, если оно делится только само на себя и на 1)
function isPrimeNumbers(num) {
    if ((!num && num !== 0) || num < 0) {
        return "invalid incoming data";
    };

    for (let i = 2; i < num; i++) {
        if (num % i == 0) return false;
    }

    return num > 1;
}
console.log('2) ', isPrimeNumbers(2));

// 3) Найти корень натурального числа с точностью до целого(рассмотреть вариант последовательного подбора и метод бинарного поиска)
function getSqrt(num, f = false) {
    if (!num || num < 1) {
        return "invalid incoming data";
    };

    let x,
        x1 = num / 2;

    while (x !== x1) {
        x = x1;
        x1 = (x + (num / x)) / 2;
    }

    if (f) {
        return x;
    } else {
        return (x % 2 < 1 && x % 2 != 0) ? x - (x % 2) : x;
    }

}

console.log('3) ', getSqrt(121));

// 4) Вычислить факториал числа
function getFuck(num) {
    if (!+num || num < 1) {
        return "invalid incoming data";
    };

    let f = 1;

    for (let i = 1; i < num; i++) {
        f *= i + 1;
    }

    return f;
}
console.log('4) ', getFuck(5));

// 5) Посчитать сумму цифр заданного числа
function getNumSum(num) {
    if (!+num && num !== 0) return "invalid incoming data";

    let str = '' + num,
        sum = 0,
        end = (num < 0) ? 1 : 0;

    for (let i = end; i < str.length; i++) {
        sum += +str[i];
    }

    return end ? +`-${sum}` : +sum;
}
console.log('5) ', getNumSum(666));

// 6) Вывести число, которое является зеркальным отображением последовательности цифр заданного числа
function getMirrorNum(num) {
    if (!+num && num !== 0) return "invalid incoming data";

    let str = '' + num,
        numMirror = '',
        end = (num < 0) ? 1 : 0;

    for (let i = str.length - 1; i >= end; i--) {
        numMirror += str[i];
    }

    return end ? +`-${numMirror}` : +numMirror;
}
console.log('6) ', getMirrorNum(123));

// Одномерные массивы
//----------------------------------------------------------------------------------------
console.log('Одномерные массивы:');

// 1, 3) Найти минимальный элемент массива и его индекс
function findMinArr(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';

    let min = arr.length ? arr[0] : 0,
        index = 0;

    for (let i = 0; i < arr.length; i++) {
        if (min > arr[i]) {
            min = arr[i];
            index = i;
        }
    }

    return {
        min,
        index
    };
}
console.log('1, 3) ', findMinArr([1, 20, 3, -4, 5, 6]));

// 2, 4) Найти максимальный элемент массива и его индекс
function findMaxArr(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';

    let max = 0,
        index = 0;

    for (let i = 0; i < arr.length; i++) {
        if (max < arr[i]) {
            max = arr[i];
            index = i;
        }
    }

    return {
        max,
        index
    };
}
console.log('2, 4) ', findMaxArr([1, 20, 3, -4, 5, 6]));

// 5) Посчитать сумму элементов массива с нечетными индексами 
function getOddIndexSum(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';

    let sum = 0;

    for (let i = 0; i < arr.length; i++) {
        if (i % 2) sum += arr[i];
    }

    return sum;
}
console.log('5) ', getOddIndexSum([1, 2, 3, 4, 5, 6]));

// 6) Сделать реверс массива (массив в обратном направлении) 
function reverseArr(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';

    let half = (arr.length % 2) ? (arr.length - 1) / 2 : arr.length / 2;

    for (let i = 0; i < half; i++) {
        let val = arr[arr.length - 1 - i];
        arr[arr.length - 1 - i] = arr[i];
        arr[i] = val;
    }

    return arr;
}
console.log('6) ', reverseArr([2, 3, 4, 5, 6, 7, 8]));

// 7) Посчитать количество нечетных элементов массива
function getOddCount(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';

    let counter = 0;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] % 2) counter++;
    }

    return counter;
}
console.log('7) ', getOddCount([5, 5, 2, 2, 22, 5, 8, 6, 2, 1]));

// 8) Поменять местами первую и вторую половину массива, например, для массива
function swapHalves(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';

    let half, right;

    if (arr.length % 2) {
        half = (arr.length - 1) / 2;
        right = half + 1;
    } else {
        half = arr.length / 2;
        right = half;
    }

    for (let i = 0; i < half; i++) {
        let val = arr[right];
        arr[right] = arr[i];
        arr[i] = val;
        right++;
    }

    return arr;
}
console.log('8) ', swapHalves([8, 8, 1, 4]), swapHalves([8, 8, ':', 1, 4]));

// 9) Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert)) 
function sortBubble(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';
    // сравниваем пару соседних элементов и выстраиваем их в правильном порядке, повторяем это arr.length - 1 - i
    for (let i = 0; i < arr.length - 1; i++) {
        for (let j = 0; j < arr.length - 1 - i; j++) {
            if (arr[j + 1] < arr[j]) {
                let val = arr[j + 1];

                arr[j + 1] = arr[j];
                arr[j] = val;
            }
        }
    }

    return arr;
}

function sortSelect(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';
    // при каждой итерации выбрать минимальное значение и поменять его местами с первым значением еще не отсортированного участка и уменьшать этот участок на один
    for (let i = 0; i < arr.length - 1; i++) {
        let min = i,
            val;

        for (let j = i + 1; j < arr.length; j++) {
            if (arr[j] < arr[min]) min = j;
        }

        val = arr[min];
        arr[min] = arr[i];
        arr[i] = val;
    }

    return arr;
}

function sortInsert(arr) {
    if(!Array.isArray(arr)) return 'invalid incoming data';
    // поочередно сравниваем каждое значение массива со значениями стоящими перед ним и если оно мешьме предидущего в ставляем текущее значение перед предидущим.
    for (let i = 1; i < arr.length; i++) {
        const current = arr[i];
        let j = i;

        while (j > 0 && arr[j - 1] > current) {
            arr[j] = arr[j - 1];
            j--;
        }

        arr[j] = current;
    }

    return arr;
}

console.log('9) ', sortBubble([2, 6, 78, 1, 3, 5, 7, -10]), sortSelect([2, 6, 78, 1, 3, 5, 7, -10]), sortInsert([2, 6, 78, 1, 3, 5, 7, -10]));
// Функции
//----------------------------------------------------------------------------------------
console.log('Функции:');

// 1) Получить строковое название дня недели по номеру дня. 
function getDay(num) {
    if (!num || num < 0 || num > 7) return "invalid incoming data";

    return ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'][num - 1];
}
console.log('1) ', getDay(1), getDay(3), getDay(7));

// 2) Найти расстояние между двумя точками в двухмерном декартовом пространстве.
function getPower(num) {
    if (typeof num !== 'number') return "invalid incoming data";

    return num * num;
}

function getLength(x1, x2, y1, y2) {
    if (x1 < 0 || x2 < 0 || y1 < 0 || y2 < 0) return 'invalid incoming data';

    return getSqrt(
        getPower(x2 - x1) + getPower(y2 - y1),
        true
    );
}
console.log('2) ', getLength(10, 40, 10, 40));