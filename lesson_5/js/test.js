mocha.setup('bdd');

const assert = chai.assert;

describe('backspace', function() {
    it('should return [] if an agrument is [1]', function() {
        const arr = [1];

        const actual = backspace(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return ["2"] if an agrument is ["22"]', function() {
        const arr = ['22'];

        const actual = backspace(arr);

        const expected = ['2'];
        assert.deepEqual(actual, expected);
    });

    it('should return [3] if an agrument is [33]', function() {
        const arr = [33];

        const actual = backspace(arr);

        const expected = ['3'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["4"] if an agrument is ["4", "+"]', function() {
        const arr = ['4', '+'];

        const actual = backspace(arr);

        const expected = ['4'];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an agrument is []', function() {
        const arr = [];

        const actual = backspace(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an agrument is invalid', function() {
        const arr = undefined;

        const actual = backspace(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });
});

describe('updateDisplay', function() {
    it('should return "111" if an argument is ["111"]', function() {
        const arr = ['111'];

        const actual = updateDisplay(arr);

        const expected = '111';
        assert.deepEqual(actual, expected);
    });

    it('should return "111+222" if an argument is ["111", "+", "222"]', function() {
        const arr = ['111', '+', '222'];

        const actual = updateDisplay(arr);

        const expected = '111+222';
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is invalid', function() {
        const arr = [];

        const actual = updateDisplay(arr);

        const expected = '';
        assert.deepEqual(actual, expected);
    });

    it('should return an apmpty sting if an argument is invalid', function() {
        const arr = undefined;

        const actual = updateDisplay(arr);

        const expected = '';
        assert.deepEqual(actual, expected);
    });
});

describe('updateOutput', function () {    
    it('should return ["5"] if arguments are ([], "5")', function() {
        const arg1 = [],
            arg2 = '5';

        const actual = updateOutput(arg1, arg2);

        const expected = ['5'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51"] if arguments are (["5"], "1")', function() {
        const arg1 = ['5'],
            arg2 = '1';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*"] if arguments are (["51"], "*")', function() {
        const arg1 = ['51'],
            arg2 = '*';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*", "6"] if arguments are (["51", "*"], "6")', function() {
        const arg1 = ['51', '*'],
            arg2 = '6';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*', '6'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*", "6", "+"] if arguments are (["51","*", "6", "+"], "+")', function() {
        const arg1 = ['51', '*', '6', '+'],
            arg2 = '+';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*', '6', '+'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["51","*", "6."] if arguments are (["51","*", "6"], ".")', function() {
        const arg1 = ['51', '*', '6'],
            arg2 = '.';

        const actual = updateOutput(arg1, arg2);

        const expected = ['51', '*', '6.'];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if the first argument is invalid', function() {
        const arg1 = undefined,
            arg2 = '';

        const actual = updateOutput(arg1, arg2);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return incoming array if the last argument is invalid', function() {
        const arg1 = ['2','3'],
            arg2 = undefined;

        const actual = updateOutput(arg1, arg2);

        const expected = ['2','3'];
        assert.deepEqual(actual, expected);
    });
});

describe('calculateData', function() {
    it('should return ["4"] if an argument is ["2", "*", "2"]', function() {
        const arr = ['2', '*', '2'];

        const actual = calculateData(arr);

        const expected = [2 * 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["8"] if an argument is ["2", "*", "2", "*", "2"]', function() {
        const arr = ['2', '*', '2', '*', '2'];

        const actual = calculateData(arr);

        const expected = [2 * 2 * 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["2"] if an argument is ["2", "/", "2", "*", "2"]', function() {
        const arr = ['2', '/', '2', '*', '2'];

        const actual = calculateData(arr);

        const expected = [2 / 2 * 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["3"] if an argument is ["2", "/", "2", "+", "2"]', function() {
        const arr = ['2', '/', '2', '+', '2'];

        const actual = calculateData(arr);

        const expected = [2 / 2 + 2 + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return ["-13"] if an argument is ["-2", "*", "-33", "/", "5.5", "+", "-1"]', function() {
        const arr = ['2', '*', '-33', '/', '5.5', '+', '-1'];

        const actual = calculateData(arr);

        const expected = [2 * -33 / 5.5 + -1  + ""];
        assert.deepEqual(actual, expected);
    });

    it('should return [] if an argument is []', function() {
        const arr = [];

        const actual = calculateData(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return ["Error!"] if an argument is invalid', function() {
        const arr = undefined;

        const actual = calculateData(arr);

        const expected = ["Error!"];
        assert.deepEqual(actual, expected);
    });
})

describe('filterArray', function() {
    it('should return ["1"] if an argument is [null, null, null, 1]', function() {
        const arr = [null, null, null, 1];

        const actual = filterArray(arr);

        const expected = ['1'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["1", "+", "2"] if an argument is [null, null, null, 1, "+", "2"]', function() {
        const arr = [null, null, null, 1, '+', '2'];

        const actual = filterArray(arr);

        const expected = ['1', '+', '2'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["2", "-", "1", "+", "2"] if an argument is ["2", "+", null, null, null, 1, "+", "2"]', function() {
        const arr = ['2', '-', null, null, null, 1, '+', '2'];

        const actual = filterArray(arr);

        const expected = ['2', '-', '1', '+', '2'];
        assert.deepEqual(actual, expected);
    });
    
    it('should return [] if an argument is []', function() {
        const arr = [];

        const actual = filterArray(arr);

        const expected = [];
        assert.deepEqual(actual, expected);
    });

    it('should return ["Error!"] if an argument is [NaN, null, null, 1]', function() {
        const arr = [NaN, null, null, 1];

        const actual = filterArray(arr);

        const expected = ['Error!'];
        assert.deepEqual(actual, expected);
    });

    it('should return ["Error!"] if an argument is invalid', function() {
        const arr = undefined;

        const actual = filterArray(arr);

        const expected = ['Error!'];
        assert.deepEqual(actual, expected);
    });
})

describe('getFactorial', function() {
    it('should return "Error!" if an argument is empty', function() {
        const actual = getFactorial();

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return "Error!" if an argument is "-10"', function() {
        const data = "-10";

        const actual = getFactorial(data);

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return "Error!" if an argument is "10.1"', function() {
        const data = "10.1";

        const actual = getFactorial(data);

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return 120 if an argument is "5"', function() {
        const data = "5";

        const actual = getFactorial(data);

        const expected = 120;
        assert.equal(actual, expected);
    });
});

describe('getSqrt', function() {
    it('should return "Error!" if an argument is empty', function() {
        const actual = getSqrt();

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return "Error!" if an argument is "-10"', function() {
        const data = "-10";

        const actual = getFactorial(data);

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return 9 if an argument is "81"', function() {
        const data = "81";

        const actual = getSqrt(data);

        const expected = 9;
        assert.equal(actual, expected);
    });
});

describe('getLn', function() {
    it('should return "Error!" if an argument is empty', function() {
        const actual = getLn();

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return "Error!" if an argument is "-10"', function() {
        const data = "-10";

        const actual = getLn(data);

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return 2.302585092994046 if an argument is "10"', function() {
        const data = "10";

        const actual = getLn(data);

        const expected = 2.302585092994046;
        assert.equal(actual, expected);
    });
});

describe('getLog', function() {
    it('should return "Error!" if an argument is empty', function() {
        const actual = getLog();

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return "Error!" if an argument is "-10"', function() {
        const data = "-10";

        const actual = getLog(data);

        const expected = "Error!";
        assert.equal(actual, expected);
    });

    it('should return 2 if an argument is "100"', function() {
        const data = "100";

        const actual = getLog(data);

        const expected = 2;
        assert.equal(actual, expected);
    });
});

mocha.run();