const calc = document.getElementById('calc'),
    display = document.getElementById('calcDisplay');

let output = [];

calc.addEventListener('click', function (event) {
    const target = event.target;
        btn = target.dataset.btn;

    if (btn == 'sign') {
        output = updateOutput(output, target.value);
    };

    if (target.dataset.btn == 'remove') {
        output = backspace(output);
    };

    if (target.dataset.btn == 'change-sign') {
        output = changeSign(output);
    };

    if (target.dataset.btn == 'result') {
        output = calculateData(output);
    };

    if (target.dataset.btn == 'x!') {
        const data = calculateData(output)[0];
        
        output = [getFactorial(data)];
    };

    if (target.dataset.btn == 'sqrt') {
        const data = calculateData(output)[0];
        
        output = [getSqrt(data)];
    };

    if (target.dataset.btn == 'ln') {
        const data = calculateData(output)[0];
        
        output = [getLn(data)];
    };

    if (target.dataset.btn == 'log') {
        const data = calculateData(output)[0];
        
        output = [getLog(data)];
    };

    if (target.dataset.btn == 'p') {
        output = updateOutput(output, Math.PI.toFixed(4));
    };

    if (target.dataset.btn == 'e') {
        output = updateOutput(output, Math.E.toFixed(4));
    };

    console.log(output);
    display.textContent = updateDisplay(output);
});


function changeSign(arr) {
    if (!Array.isArray(arr) || !arr.length) return [];

    const last = arr[arr.length - 1];

    if (+last || +last === 0) {
        if (+last < 0) {
            let str = '';
            
            for(let i = 1; i < last.length; i++) {
                str += last[i];
            };

            arr[arr.length - 1] = str;
        } else {
            arr[arr.length - 1] = '-' + last;
        };
    };

    return arr;
}

function backspace(arr) {
    if (!Array.isArray(arr) || !arr.length) return [];

    const last = String(arr[arr.length - 1]);

    if (last.length > 1) {
        let str = '';

        for (let i = 0; i < last.length - 1; i++) {
            str += last[i];
        };

        arr[arr.length - 1] = str;

        return arr;
    } else {
        let newArr = [];

        for (let i = 0; i < arr.length - 1; i++) {
            newArr[newArr.length] = arr[i];
        };

        return newArr;
    }
}

function updateDisplay(arr) {
    if (!Array.isArray(arr)) return '';

    let str = '';

    for (let i = 0; i < arr.length; i++) {
        str += arr[i];
    }

    return str;
}

function updateOutput(arr, item) {
    if(!Array.isArray(arr)) return [];
    if(!item) return arr;

    const last = arr[arr.length - 1];

    if((+item || +item === 0)) {
        if(arr.length && (+last || +last === 0)) {
            arr[arr.length - 1] += item;
        } else {
            arr[arr.length] = item;
        };
    } else if (+last || +last === 0) {
        if(item === '.' && last[last.length - 1] !== '.') {
            arr[arr.length - 1] += item;
        } else {
            arr[arr.length] = item;
        };
    };

    return arr;
}

function calculateData(arr) {
    if (!Array.isArray(arr)) return ['Error!'];

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === '/') {
            arr[i + 1] = +arr[i - 1] / +arr[i + 1];
            arr[i] = null;
            arr[i - 1] = null;
        };
        if (arr[i] === '*') {
            arr[i + 1] = +arr[i - 1] * +arr[i + 1];
            arr[i] = null;
            arr[i - 1] = null;
        };
    }

    arr = filterArray(arr);

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === '-') {
            arr[i + 1] = +arr[i - 1] - +arr[i + 1];
            arr[i] = null;
            arr[i - 1] = null;
        };
        if (arr[i] === '+') {
            arr[i + 1] = +arr[i - 1] + +arr[i + 1];
            arr[i] = null;
            arr[i - 1] = null;
        };
    }

    arr = filterArray(arr);
    
    return arr;
}

function filterArray(arr) {
    if (!Array.isArray(arr)) return ['Error!'];

    let newArr = [];

    for (let i = 0; i < arr.length; i++) {
        if ((!arr[i] && arr[i] !== null) || arr[i] === Infinity) return ['Error!'];
        if (arr[i] || arr[i] === 0) newArr[newArr.length] = String(arr[i]);
    }

    return newArr;
};

function getFactorial(num) {
    if (!+num || +num < 0 || +num % 1 !== 0) return "Error!";

    return (function factorial(num) {
        return num ? num * factorial(num - 1) : 1;
    })(+num);
}

function getSqrt(num) {
    if (!+num || +num < 0) return "Error!";

    return Math.sqrt(num);
}

function getLn(num) {
    if (!+num || +num < 0) return "Error!";

    return Math.log(num);
};

function getLog(num) {
    if (!+num || +num < 0) return "Error!";

    return Math.log10(num);
};