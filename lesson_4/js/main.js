const text = document.getElementById('textArea'),
    content = document.getElementById('content');

window.addEventListener('click', function(event) {
    if (event.target.id === 'visualizeBtn') {
        const data = renderData(JSON.parse(text.value));
        content.replaceChild(data, content.lastElementChild);
    };

    if (event.target.classList.contains('header')) {
        toggleDropdown(event.target);
    };
});

function toggleDropdown(elem) {
    if (!elem || !elem.classList) return;

    if (elem.classList.contains('opened')) {
        elem.classList.remove('opened');
    } else {
        elem.classList.add('opened');
    };
};

function renderData(obj, bg = true) {
    if (typeof obj !== 'object') return;

    const container = document.createElement('div');

    container.classList.add('dropdown', bg ? 'red' : 'green');
 
    for (const key in obj) {
        if (typeof obj[key] === 'object') {
            const header = document.createElement('div');

            header.classList.add('header', !bg ? 'red' : 'green');
            header.textContent = key;
            container.appendChild(header);
            container.appendChild(renderData(obj[key], !bg));
        } else {
            const row = document.createElement('div');

            row.classList.add('row');
            row.textContent = `${key}: ${obj[key]}`;
            container.appendChild(row);
        };
    };

    return container;
};