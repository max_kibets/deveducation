mocha.setup('bdd');

const assert = chai.assert;

describe('toggleDropdown', function() {
    it('should return undefined if an argument is invalid', function() {
        const actual = toggleDropdown();

        const expected = undefined;
        assert.equal(actual, expected);
    });

    it('should return undefined if an argument has not classList', function() {
        const data = {};

        const actual = toggleDropdown(data);

        const expected = undefined;
        assert.equal(actual, expected);
    });

    it('classList should equal "header opened" if it was "header"', function() {
        const div = document.createElement('div');
        div.classList.add('header');

        toggleDropdown(div);
        const actual = div.classList.value;

        const expected = 'header opened';
        assert.equal(actual, expected);
    });

    it('classList should equal "header" if it was "header opened"', function() {
        const div = document.createElement('div');
        div.classList.add('header', 'opened');

        toggleDropdown(div);
        const actual = div.classList.value;

        const expected = 'header';
        assert.equal(actual, expected);
    });
});

describe('renderData', function() {
    it('should return undefined if an argument is empty', function() {
        const actual = renderData();

        const expected = undefined;
        assert.equal(actual, expected);
    });

    it('should return undefined if an argument is not object', function() {
        const data = 'not obj';

        const actual = renderData(data);

        const expected = undefined;
        assert.equal(actual, expected);
    });
});

mocha.run();