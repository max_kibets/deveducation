const socket = new WebSocket('ws://localhost:8081'),
  popup = document.getElementById('popup'),
  chat = document.getElementById('chat');
  historyContainer = document.getElementById('history');
  subsrcibersContainer = document.getElementById('subsrcibers');
  currentUser = {
    type: null,
    name: null,
    message: null,
    room: null
  };

popup.addEventListener('click', onLogIn);
chat.addEventListener('click', onChatClick);
socket.addEventListener('message', onMessage);

// EVENT HANDLERS
function onLogIn(event) {
  const authInput = this.querySelector('input');

  if (authInput.value && event.target.tagName === 'BUTTON') {
    sendAuth(authInput.value);
  }
}

function onChatClick(event) {
  if (event.target.tagName === 'BUTTON') {
    const textarea = this.querySelector('textarea');
    sendMessage(textarea);
  }

  if (event.target.dataset.room) {
    toggleRoom(event.target);
    sendRoom(event.target);
  }
}

function onMessage(message) {
  const event = JSON.parse(message.data);

  if (event.type === 'authorize') {
    hidePopup(popup);
  }

  if (event.type === 'updateSub' && event.room === currentUser.room) {
    showSubscribers(event.sub);
  }

  if (event.type === 'message' && event.room === currentUser.room) {
    showMessage(event);
  };
}

// SEND FUNCTIONS
function sendAuth(text) {
  currentUser.type = 'authorize';
  currentUser.name = text;

  socket.send(JSON.stringify(currentUser));
}

function sendMessage(node) {
  if (!node.value) return void 0;

  currentUser.type = 'message';
  currentUser.message = node.value;
  
  if (currentUser.name && currentUser.room) {
    socket.send(JSON.stringify(currentUser));
    node.value = '';
  }
}

function sendRoom(node) {
  currentUser.type = 'changeRoom';
  currentUser.room = node.dataset.room;
  socket.send(JSON.stringify(currentUser));
}

// CREATE DOM FUNCTIONS
function showMessage(user) {
  const msg = createMessage(user);

  msg && historyContainer.appendChild(msg);

  scrollBottom(historyContainer.parentElement);
}

function createMessage({name, message}) {
  if (!name || !message) return null;

  const container = document.createElement('div');
  
  container.classList.add('msg');
  container.innerHTML = `<div class='msg__name'>${name}</div><div class='msg__text'>${message}</div>`;

  return container;
}

function showSubscribers(subscribers) {
  subsrcibersContainer.innerHTML = '';

  if (subscribers.length) {
    for (const sub of subscribers) {
      const node = createSubscriber(sub);

      node && subsrcibersContainer.appendChild(node);
    }
  }
}

function createSubscriber({name, status}) {
  if (!name || !status) return null;

  const container = document.createElement('div');

  container.classList.add('user');
  container.innerHTML = `<span class="${status}"></span> ${name}`;

  return container;
}

// HALPERS
function hidePopup(node) {
  if (!(node instanceof Node)) return void 0;

  node.classList.add('popup--hidden');
  node.removeEventListener('click', onLogIn);
}

function toggleRoom(node) {
  if (!(node instanceof Node)) return void 0;

  const rooms = node.parentElement.children;

  Array.prototype.forEach.call(rooms, (item) => {
    item.classList.remove('room--active');
  });

  node.classList.add('room--active');
  historyContainer.innerHTML = '';
  subsrcibersContainer.innerHTML = '';
}

function scrollBottom(elem) {
  elem.scrollTop = elem.scrollHeight;
}