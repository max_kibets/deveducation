const WebSocker = require('ws'),
  webSocketServer = new WebSocker.Server({port: 8081}),
  users = [],
  rooms = {
    room_1: {
      sub: [],
      history: []
    },
    room_2: {
      sub: [],
      history: []
    },
    room_3: {
      sub: [],
      history: []
    }
  };

webSocketServer.on('connection', (ws) => {

  ws.on('message', (message) => {
    const event = JSON.parse(message);

    if (event.type === 'authorize') {
      authHandler(ws, event);
    }

    if (event.type === 'message') {
      const subscriber = users.find(item => item.name === event.name);

      subscriber.currentRoom = event.room;

      if (subscriber && !rooms[subscriber.currentRoom].sub.includes(subscriber)) {
        subscribe(subscriber);
      }
      
      broadcast(event);
      rooms[event.room].history.push(event);
    }

    if (event.type === 'changeRoom') {
      udateClientRoom(event);
    }
  });

  ws.on('close', () => {
    users.forEach(item => {
      if (item.ws === ws) {
        item.status = 'offline';
      }
    });

    udateAllClienSubscribers();
  });
});

function authHandler(ws, user) {
  const registredUser = users.find(item => item.name === user.name);

  if (registredUser) {
    registredUser.ws = ws;
    registredUser.status = 'online';
    udateAllClienSubscribers();
  } else {
    users.push({
      ws: ws,
      name: user.name,
      status: 'online'
    });
  }

  ws.send(JSON.stringify({type: 'authorize'}));
}

function broadcast(user) {
  users.forEach(item => {
    if (item.currentRoom === user.room) {
      item.ws.send(JSON.stringify({
        type: 'message',
        name: user.name,
        message: user.message,
        room: user.room
      }));
    }
  })
}

function udateClientRoom(user) {
  const currentUser = users.find(item => item.name === user.name);

  const isSubscriber = rooms[user.room].sub.find(item => item.name === user.name);

  if (isSubscriber) {
    for (const msg of rooms[user.room].history) {
      currentUser.ws.send(JSON.stringify({
        type: 'message',
        name: msg.name,
        message: msg.message,
        room: user.room
      }));
    }
  
    currentUser.ws.send(JSON.stringify({
      type: 'updateSub',
      sub: rooms[user.room].sub,
      room: user.room
    }));
  }
}

function subscribe(user) {
  rooms[user.currentRoom].sub.push(user);

  for (const msg of rooms[user.currentRoom].history) {
    user.ws.send(JSON.stringify({
      type: 'message',
      name: msg.name,
      message: msg.message,
      room: user.currentRoom
    }));
  }

  user.ws.send(JSON.stringify({
    type: 'updateSub',
    sub: rooms[user.currentRoom].sub,
    room: user.currentRoom
  }));

  udateAllClienSubscribers();
}

function udateAllClienSubscribers() {
  users.forEach(item => {
    item.ws.send(JSON.stringify({
      type: 'updateSub',
      sub: item.currentRoom ? rooms[item.currentRoom].sub : null,
      room: item.currentRoom
    }));
  });
}