const WebSocker = require('ws'),
  webSocketServer = new WebSocker.Server({port: 8081}),
  users = [],
  history = {},
  defaultRooms = ['room_1'];

defaultRooms.forEach(room => history[room] = []);

webSocketServer.on('connection', (ws) => {
  ws.on('message', (message) => {
    const event = JSON.parse(message);

    if (event.type === 'authorize') {
      authHandler(ws, event.user);
    }

    if (event.type === 'update_room') {
      updateRoomHandler(event.user);
    }

    if (event.type === 'message') {
      messageHandler(event.user);
    }

    if (event.type === 'get_sub') {
      sendSubscribers(event.user);
    }

    if (event.type === 'subscribe') {
      const subscriber = users.find(user => user.data.name === event.subscriber);
      subscribeUser(subscriber, event.user.currentRoom);
    }

    if (event.type === 'create_room') {
      createPrivateRoom(event.user, event.roomName);
    }

    if (event.type === 'leave_room') {
      leaveRoom(event.leaveRoom, event.user);
    }

    if (event.type === 'remove_message') {
      removeMessage(event.index, event.user);
    }

    if (event.type === 'edit_message') {
      editMessage(event.index, event.user);
    }
  });

  ws.on('close', () => {
    const disconnectedUser = users.find(user => user.ws === ws);

    if (disconnectedUser) {
      disconnectedUser.data.status = 'offline';
      disconnectedUser.data.currentRoom = null;

      updateRoomSubsAll(disconnectedUser);
      
      console.log(`user ${disconnectedUser.data.name} ${disconnectedUser.data.status}`);
    }
  });
});

function authHandler(ws, client) {
  const registredUser = users.find(user => user.data.name === client.name);

  if (registredUser) {
    if (registredUser.data.status !== 'online') {
      registredUser.ws = ws;
      registredUser.data.currentRoom = 'room_1';
      registredUser.data.status = 'online';

      broadcast('authorize', ws, registredUser.data);

      updateRoomHistory(registredUser);
      updateRoomSubsAll(registredUser);

      if (registredUser.data.subsribedRooms.size) {
        for (const room of registredUser.data.subsribedRooms.keys()) {
          if (!defaultRooms.includes(room)) {
            sendPrivateRoom(registredUser, room);
          }
        }
      };

      for (const [kay, value] of registredUser.data.subsribedRooms) {
        broadcast('unread_message', ws, {
          currentRoom: kay,
          unread: value.unread
        });
      }

      console.log(`user ${registredUser.data.name} ${registredUser.data.status}`);
    } else {
      console.log(`user ${registredUser.data.name} already ${registredUser.data.status}`);
      return;
    }
  }

  if (!registredUser) {
    const newUser = {
      ws: ws,
      data: {
        status: 'online',
        name: client.name,
        currentRoom: 'room_1',
        subsribedRooms: new Map()
      }
    };

    users.push(newUser);

    ws.send(JSON.stringify({
      type: 'authorize',
      data: newUser.data
    }));

    updateRoomHistory(newUser);
    updateRoomSubs(newUser);
    console.log(`new user ${newUser.data.name} ${newUser.data.status}`);
  }
}

function messageHandler(client) {
  const registredUser = users.find(user => user.data.name === client.name);

  // subscribe if need
  if (!registredUser.data.subsribedRooms.has(registredUser.data.currentRoom)) {
    subscribeUser(registredUser, registredUser.data.currentRoom);
  }

  //save history
  const message = (({name, message}) => {return {name, message}})(client);
  registredUser.data.message = client.message;
  history[registredUser.data.currentRoom].push(message);

  // send message for all in room
  broadcastMessage(registredUser);

  console.log(`user ${registredUser.data.name} sent a message`);
}

function updateRoomHandler(client) {
  const registredUser = users.find(user => user.data.name === client.name);

  registredUser.data.currentRoom = client.currentRoom;

  updateRoomHistory(registredUser);
  
  updateRoomSubs(registredUser);

  if (registredUser.data.subsribedRooms.has(registredUser.data.currentRoom)) {
    registredUser.data.subsribedRooms.get(registredUser.data.currentRoom).unread = 0;
  }

  broadcast('unread_message', registredUser.ws, {
    currentRoom: registredUser.data.currentRoom,
    unread: 0
  });

  console.log(`user ${registredUser.data.name} changed room to ${registredUser.data.currentRoom}`);
}

function broadcastMessage(registredUser) {
  for (const user of users) {
    if (user.data.currentRoom === registredUser.data.currentRoom) {
      broadcast('message', user.ws, registredUser.data);
    } else if (user.data.subsribedRooms.has(registredUser.data.currentRoom)) {
      user.data.subsribedRooms.get(registredUser.data.currentRoom).unread++;

      if (user.data.status === 'online') {
        broadcast('unread_message', user.ws, {
          currentRoom: registredUser.data.currentRoom,
          unread: user.data.subsribedRooms.get(registredUser.data.currentRoom).unread
        });
      }
    }
  }
}

function updateRoomHistory(registredUser) {  
  broadcast('update_history', registredUser.ws, history[registredUser.data.currentRoom]);
}

function updateRoomSubsAll(registredUser) {
  const subscribers = users.filter(user => registredUser.data.subsribedRooms.has(user.data.currentRoom));

  subscribers.forEach(user => updateRoomSubs(user));
}

function updateRoomSubs(registredUser) {
  let subscribers = users.filter(user => user.data.subsribedRooms.has(registredUser.data.currentRoom));

  subscribers = subscribers.map(user => user.data);

  if (subscribers.length) broadcast('update_sub', registredUser.ws, subscribers);
}

function broadcast(type, ws, data) {
  ws.send(JSON.stringify({
    type,
    data
  }));
}

function sendSubscribers(client) {
  const registredUser = users.find(user => user.data.name === client.name);

  let subscribers = users.filter(user => !user.data.subsribedRooms.has(registredUser.data.currentRoom));

  if (subscribers.length && registredUser.data.subsribedRooms.has(registredUser.data.currentRoom)) {
    subscribers = subscribers.map(user => user.data);

    registredUser.ws.send(JSON.stringify({
      type: 'sub_list',
      data: subscribers
    }));

    console.log(`sub list sent`);
  }
}

function subscribeUser(registredUser, room) {
  registredUser.data.subsribedRooms.set(room, {unread: 0});

  updateRoomSubsAll(registredUser);

  if (!defaultRooms.includes(room)) {
    sendPrivateRoom(registredUser, room);
  }

  console.log(`user ${registredUser.data.name} subscribed to ${room}`);
}

function createPrivateRoom(client, roomName) {
  const registredUser = users.find(user => user.data.name === client.name),
    room = roomName.split(' ').join('_');
  
  if (!Object.keys(history).includes(room)) {
    registredUser.data.subsribedRooms.set(room, {unread: 0});
    history[room] = [];

    sendPrivateRoom(registredUser, room);
  
    console.log(`user ${registredUser.data.name} created room ${roomName}`);
  } else {
    console.log(`room with name ${roomName} already exists`);
  }
}

function sendPrivateRoom(registredUser, room) {
  const roomName = room.split('_').join(' ');

  registredUser.ws.send(JSON.stringify({
    type: 'create_room',
    roomName,
    room
  }));
}

function leaveRoom(room, client) {
  const registredUser = users.find(user => user.data.name === client.name);
  
  registredUser.data.subsribedRooms.delete(room);
  registredUser.data.currentRoom = defaultRooms[0];

  const subscribers = users.filter(user => user.data.subsribedRooms.has(room));

  if (subscribers.length) {
    users.forEach(user => {
      if (user.data.currentRoom === room) {
        updateRoomSubs(user);
      }
    });

    console.log(`${subscribers.length} sub left`);
  } else {
    delete history[room];
    console.log(`no sub left`);
  };

  console.log(`user ${registredUser.data.name} left ${room}`);
}

function removeMessage(index, client) {
  history[client.currentRoom].splice(index, 1);

  for (const user of users) {
    if (user.data.currentRoom === client.currentRoom) {
      updateRoomHistory(user);
    }
  }

  console.log(`message ${index} in room ${client.currentRoom} removed`);
}

function editMessage(index, client) {
  const msg = {
    name: client.name,
    message: client.message
  }

  history[client.currentRoom].splice(index, 1, msg);

  for (const user of users) {
    if (user.data.currentRoom === client.currentRoom && user.data.name !== client.name) {
      updateRoomHistory(user);
    }
  }

  console.log(`user ${client.name} edited msg ${index} in room ${client.currentRoom}: ${client.message}`);
}