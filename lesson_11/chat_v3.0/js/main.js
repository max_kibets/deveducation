const socket = new WebSocket('ws://localhost:8081'),
  popup = document.getElementById('popup'),
  chat = document.getElementById('chat'),
  historyContainer = chat.querySelector('#history'),
  subscribersContainer = chat.querySelector('#subsrcibers'),
  rooms = chat.querySelector('#rooms'),
  defaultRoom = rooms.querySelector('[data-room=room_1]'),
  inviteBtn = chat.querySelector('[data-dropdown="invite"]'),
  invitePopup = chat.querySelector('#invitePopup'),
  invitePopupContainer = invitePopup.querySelector('.dropdown__content'),
  createRoomBtn = chat.querySelector('[data-dropdown="addRoom"]'),
  addRoomPopup = createRoomBtn.querySelector('#addRoomPopup');

let currentUser = {
    name: null,
    message: null
  },
  editingMessage = false;

popup.addEventListener('click', onLogIn);
socket.addEventListener('message', onMessage);
chat.addEventListener('click', onChatClick);
inviteBtn.addEventListener('click', onInviteClick);
createRoomBtn.addEventListener('click', onCreateRoomClick);

function onLogIn(event) {
  const authInput = this.querySelector('input');

  if (authInput.value && event.target.tagName === 'BUTTON') {
    requestAuth(authInput.value);
  }
}

function onMessage(message) {
  const event = JSON.parse(message.data);

  if (event.type === 'authorize') {
    currentUser = event.data;
    hidePopup(popup);
    popup.removeEventListener('click', onLogIn);
  }

  if (event.type === 'update_sub') {
    showSubscribers(subscribersContainer, event.data);
  }

  if (event.type === 'message') {
    showMessage(historyContainer, event.data);
  }

  if (event.type === 'update_history') {
    showMessage(historyContainer, event.data);
  }

  if (event.type === 'sub_list') {
    showSubscribers(invitePopupContainer, event.data);
    showPopup(invitePopup);
  }

  if (event.type === 'create_room') {
    showRoom(rooms, event);
  }

  if (event.type === 'unread_message') {
    addUnreadMessage(rooms, event.data);
  }
}

function onChatClick(event) {
  if (event.target.dataset.btn === 'send') {
    const textarea = this.querySelector('textarea');
    sendMessage(textarea);
  }

  if (event.target.dataset.room) {
    changeRoom(event.target);
  }

  if (event.target.dataset.leave) {
    leaveRoom(event.target);
  }

  if (event.target.dataset.msg === 'remove') {
    removeMessage(event.target.parentElement);
  }

  if (event.target.dataset.msg === 'edit') {
    editMessage(event.target.parentElement);
  }
}

function onInviteClick(event) {
  if (event.target.classList.contains('dropdown')) {
    socket.send(JSON.stringify({
      type: 'get_sub',
      user: currentUser
    }));
  }
  

  if (event.target.classList.contains('dropdown__substrate')) {
    hidePopup(invitePopup);
  }

  if (event.target.classList.contains('user')) {
    socket.send(JSON.stringify({
      type: 'subscribe',
      user: currentUser,
      subscriber: event.target.textContent
    }));

    hidePopup(invitePopup);
  }
}

function onCreateRoomClick(event) {
  if (event.target.dataset.btn === 'create') {
    const input = this.querySelector('.input');

    if (input.value) {
      socket.send(JSON.stringify({
        type: 'create_room',
        user: currentUser,
        roomName: input.value
      }));
    }

    hidePopup(addRoomPopup);
  }

  if (event.target.classList.contains('dropdown')) {
    showPopup(addRoomPopup);
  }

  if (event.target.classList.contains('dropdown__substrate')) {
    hidePopup(addRoomPopup);
  }
}

function requestAuth(login) {
  currentUser.name = login;

  socket.send(JSON.stringify({
    type: 'authorize',
    user: currentUser
  }));
}

function requestUpdateRoom() {
  socket.send(JSON.stringify({
    type: 'update_room',
    user: currentUser
  }));
}

function sendMessage(node) {
  if (!node.value) return void 0;

  if (currentUser.name && currentUser.currentRoom) {
    currentUser.message = node.value;

    socket.send(JSON.stringify({
      type: 'message',
      user: currentUser
    }));

    node.value = '';

    console.log('sent message');
  }
}

function showMessage(container, data) {
  if (Array.isArray(data)) {
    container.innerHTML = '';

    data.forEach(message => {
      container.appendChild(createMessage(message));
    });
  } else {
    container.appendChild(createMessage(data));
  }

  scrollBottom(container.parentElement);
}

function createMessage({name, message}) {
  if (!name || !message) return void 0;

  const container = document.createElement('div');

  let current = '',
    edit,
    remove,
    editIcon,
    removeIcon;

  if (currentUser.name === name) {
    container.classList.add('msg--current');
    current = ' msg__name--current';

    edit = document.createElement('button'),
    remove = document.createElement('button'),
    editIcon = document.createElement('i'),
    removeIcon = document.createElement('i');

    edit.classList.add('btn', 'btn--empty', 'msg__edit');
    edit.dataset.msg = 'edit';
    editIcon.classList.add('fas', 'fa-edit');
    edit.appendChild(editIcon);
    edit.classList.msg = 'edit';
    remove.classList.add('btn', 'btn--empty', 'msg__remove');
    remove.dataset.msg = 'remove';
    removeIcon.classList.add('fas', 'fa-trash');
    remove.appendChild(removeIcon);
    remove.classList.msg = 'remove';
  }
  
  container.classList.add('msg');
  container.innerHTML = `<div class='msg__name${current}'>${name}</div><div class='msg__text'>${message}</div>`;
  
  if (edit && remove) {
    container.appendChild(edit);
    container.appendChild(remove);
  }

  return container;
}

function showSubscribers(container, subscribers) {
  if (!Array.isArray(subscribers) || !subscribers.length) return void 0;

  container.innerHTML = '';

  if (subscribers.length) {
    subscribers.forEach(sub => container.appendChild(createSubscriber(sub)));
  }
}

function createSubscriber({name, status}) {
  if (!name || !status) return null;

  const container = document.createElement('div');
  
  container.classList.add('user');

  if (currentUser.name === name) container.classList.add('user--current');

  container.innerHTML = `<span class="${status}"></span>${name}`;

  return container;
}

function addUnreadMessage(container, {currentRoom, unread}) {
  if (!currentRoom || isNaN(unread)) return void 0;

  const room = container.querySelector(`[data-room=${currentRoom}]`);

  if (unread) {
    room.dataset.unread = unread;
  } else {
    delete room.dataset.unread;
  }
}

function changeRoom(node) {
  if (!(node instanceof Node)) return void 0;

  if (!node.classList.contains('room--active')) {
    const rooms = node.parentElement.querySelectorAll('.room');
  
    Array.prototype.forEach.call(rooms, (room) => {
      room.classList.remove('room--active');
    });
  
    node.classList.add('room--active');
    delete node.dataset.unread;
    currentUser.currentRoom = node.dataset.room;
    historyContainer.innerHTML = '';
    subscribersContainer.innerHTML = '';
  
    requestUpdateRoom();
  }
}

function hidePopup(node) {
  if (!(node instanceof Node)) return void 0;

  node.classList.add('hidden');
}

function showPopup(node) {
  if (!(node instanceof Node)) return void 0;

  node.classList.remove('hidden');
}

function scrollBottom(elem) {
  elem.scrollTop = elem.scrollHeight;
}

function showRoom(container, user) {
  if (!(container instanceof Node)) return void 0;

  const node = createRoom(user);

  node && container.appendChild(node);
} 

function createRoom({roomName, room}) {
  if (!roomName || !room) return void 0;

  const node = document.createElement('div'),
    closeBtn = document.createElement('button'),
    icon = document.createElement('i');

  node.classList.add('room');
  node.textContent = roomName;
  node.dataset.room = room;
  closeBtn.classList.add('btn', 'btn--empty', 'room__close-btn');
  closeBtn.dataset.leave = room;
  icon.classList.add('fas', 'fa-sign-out-alt');
  closeBtn.appendChild(icon);
  node.appendChild(closeBtn);

  return node;
}

function leaveRoom(node) {
  const leave = confirm('Are you sure you want to leave this room?');
  
  if (leave) {
    node.parentElement.remove();

    socket.send(JSON.stringify({
      type: 'leave_room',
      leaveRoom: node.dataset.leave,
      user: currentUser
    }));

    defaultRoom.click();
  }
}

function removeMessage(node) {
  if (!(node instanceof Node)) return void 0;
  const remove = confirm('Are you sure you want to remove this message?'),
    index = Array.prototype.indexOf.call(node.parentElement.children, node);

  if (remove) {
    socket.send(JSON.stringify({
      type: 'remove_message',
      user: currentUser,
      index,
    }));
  }
}

function editMessage(node) {
  const index = Array.prototype.indexOf.call(node.parentElement.children, node),
    textarea = node.querySelector('.msg__text'),
    icon = node.querySelector('[data-msg="edit"] > i');
  
  if (textarea.isContentEditable) {
    textarea.contentEditable = false;
    icon.classList.remove('fa-check');
    icon.classList.add('fa-edit');
    currentUser.message = textarea.textContent;

    socket.send(JSON.stringify({
      type: 'edit_message',
      user: currentUser,
      index
    }));
  } else {
    textarea.contentEditable = true;
    icon.classList.remove('fa-edit');
    icon.classList.add('fa-check');
    textarea.focus();
  }
}