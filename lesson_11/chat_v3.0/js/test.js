mocha.setup('bdd');

const assert = chai.assert,
  expect = chai.expect,
  fakePupup = document.getElementById('popup').cloneNode(true),
  fakeChat = document.getElementById('chat').cloneNode(true),
  fakeHistoryContainer = fakeChat.querySelector('#history'),
  fakeSubscribersContainer = fakeChat.querySelector('#subsrcibers'),
  fakeRooms = fakeChat.querySelector('#rooms'),
  fakeInvitePopup = fakeChat.querySelector('#invitePopup');

describe('showMessage', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call scrollBottom()', () => {
    const stub = sandbox.stub(window, 'scrollBottom'),
      data = {
        name: 'name',
        message: 'message'
      }

    showMessage(fakeHistoryContainer, data);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, fakeHistoryContainer.parentElement);
  });

  it('should call createMessage() thrice', () => {
    const stub = sandbox.stub(window, 'createMessage').returns(document.createElement('div')),
      data = [
        {
          name: 'name1',
          message: 'message'
        }, {
          name: 'name2',
          message: 'message'
        }, {
          name: 'name3',
          message: 'message'
        }
      ];

    showMessage(fakeHistoryContainer, data, true);

    sandbox.assert.calledThrice(stub);
    sandbox.assert.calledWith(stub, data[0]);
    sandbox.assert.calledWith(stub, data[1]);
    sandbox.assert.calledWith(stub, data[2]);
  });

  it('should call createMessage() once', () => {
    const stub = sandbox.stub(window, 'createMessage').returns(document.createElement('div')),
      data = {
        name: 'name',
        message: 'message'
      }

    showMessage(fakeHistoryContainer, data);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, data);
  });

  it('should insert DOM element in to container', () => {
    const  data = {
      name: 'name',
      message: 'message'
    };
    fakeHistoryContainer.innerHTML = '';

    showMessage(fakeHistoryContainer, data);

    const expected = '<div id="history" class="history__inner"><div class="msg"><div class="msg__name">name</div><div class="msg__text">message</div></div></div>';
    assert.equal(fakeHistoryContainer.outerHTML, expected);
  });

  it('should insert DOM element in to container', () => {
    const  data = [{
      name: 'name',
      message: 'message'
    }, {
      name: 'name',
      message: 'message'
    }];
    fakeHistoryContainer.innerHTML = '';

    showMessage(fakeHistoryContainer, data);

    const expected = '<div id="history" class="history__inner"><div class="msg"><div class="msg__name">name</div><div class="msg__text">message</div></div><div class="msg"><div class="msg__name">name</div><div class="msg__text">message</div></div></div>';
    assert.equal(fakeHistoryContainer.outerHTML, expected);
  });
});

describe('createMessage', () => {
  before(() => {
    currentUser.name = 'name';
  });

  after(() => {
    currentUser.name = null;
  });

  it('should return undefined if an argument is invalid', () => {
    const data = {
      message: ''
    };

    const actual = createMessage(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return undefined if an argument is invalid', () => {
    const data = {
      name: ''
    };

    const actual = createMessage(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return DOM element', () => {
    const data = {
      name: 'name1',
      message: 'message'
    };

    const actual = createMessage(data).outerHTML;

    const expected = '<div class="msg"><div class="msg__name">name1</div><div class="msg__text">message</div></div>';
    assert.equal(actual, expected);
  });

  it('should return DOM element', () => {
    const data = {
        name: 'name',
        message: 'message'
      };

    const actual = createMessage(data).outerHTML;

    const expected = '<div class="msg--current msg"><div class="msg__name msg__name--current">name</div><div class="msg__text">message</div><button class="btn btn--empty msg__edit" data-msg="edit"><i class="fas fa-edit"></i></button><button class="btn btn--empty msg__remove" data-msg="remove"><i class="fas fa-trash"></i></button></div>';
    assert.equal(actual, expected);
  });
});

describe('showSubscribers', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
    currentUser.name = 'name';
  });

  after(() => {
    currentUser.name = null;
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should return undefined the second argument is invalid', () => {
    const actual = showSubscribers(fakeSubscribersContainer);

    const expected = undefined;

    assert.equal(actual, expected);
  });

  it('should call createSubscriber() thrice', () => {
    const stub = sandbox.stub(window, 'createSubscriber').returns(document.createElement('div')),
      data = [
        {
          name: 'name1',
          status: 'message'
        }, {
          name: 'name2',
          status: 'message'
        }, {
          name: 'name3',
          status: 'message'
        }
      ];

    showSubscribers(fakeSubscribersContainer, data);

    sandbox.assert.calledThrice(stub);
    sandbox.assert.calledWith(stub, data[0]);
    sandbox.assert.calledWith(stub, data[1]);
    sandbox.assert.calledWith(stub, data[2]);
  });

  it('should call createSubscriber() once', () => {
    const stub = sandbox.stub(window, 'createSubscriber').returns(document.createElement('div')),
      data = [
        {
          name: 'name',
          status: 'message'
        }
      ]

    showSubscribers(fakeSubscribersContainer, data);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, data[0]);
  });

  it('should insert DOM element in to container', () => {
    fakeSubscribersContainer.innerHTML = '';
    const data = [
        {
          name: 'name1',
          status: 'message'
        }
      ];
    
    showSubscribers(fakeSubscribersContainer, data);

    const expected = '<div id="subsrcibers"><div class="user"><span class="message"></span>name1</div></div>';
    assert.equal(fakeSubscribersContainer.outerHTML, expected);
  });

  it('should insert DOM element in to container', () => {
    fakeSubscribersContainer.innerHTML = '';
    const data = [
        {
          name: 'name',
          status: 'message'
        },
        {
          name: 'name1',
          status: 'message'
        },
        {
          name: 'name2',
          status: 'message'
        },
      ];
    
    showSubscribers(fakeSubscribersContainer, data);

    const expected = '<div id="subsrcibers"><div class="user user--current"><span class="message"></span>name</div><div class="user"><span class="message"></span>name1</div><div class="user"><span class="message"></span>name2</div></div>';
    assert.equal(fakeSubscribersContainer.outerHTML, expected);
  });
});

describe('createSubscriber', () => {
  before(() => {
    currentUser.name = 'name';
  });

  after(() => {
    currentUser.name = 'name';
  });

  it('should return undefined if an argument is invalid', () => {
    const data = {
      name: 'aa'
    };

    const actual = createSubscriber(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });
  
  it('should return undefined if an argument is invalid', () => {
    const data = {
      status: 'aa'
    };

    const actual = createSubscriber(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return DOM element', () => {
    const data = {
      name: 'name',
      status: 'online'
    };

    const actual = createSubscriber(data).outerHTML;

    const expected = '<div class="user user--current"><span class="online"></span>name</div>';
    assert.equal(actual, expected);
  });

  it('should return DOM element', () => {
    const data = {
      name: 'name1',
      status: 'online'
    };

    const actual = createSubscriber(data).outerHTML;

    const expected = '<div class="user"><span class="online"></span>name1</div>';
    assert.equal(actual, expected);
  });
});

describe('addUnreadMessage', () => {
  it('should return undefined if an argument is invalid', () => {
    const data = {
      currentRoom: 'room_1'
    };

    const actual = addUnreadMessage(fakeRooms, data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return undefined if an argument is invalid', () => {
    const data = {
      unread: '2'
    };

    const actual = addUnreadMessage(fakeRooms, data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should add data attr to container', () => {
    const data = {
      unread: 2,
      currentRoom: 'room_1'
    };

    addUnreadMessage(fakeRooms, data);
    const actual = fakeRooms.querySelector(`[data-room=${data.currentRoom}`).dataset.unread

    const expected = 2;
    assert.equal(actual, expected);
  });

  it('should remove data attr from container', () => {
    const data = {
      unread: 0,
      currentRoom: 'room_1'
    };
    fakeRooms.querySelector(`[data-room=${data.currentRoom}`).dataset.unread = 5;

    addUnreadMessage(fakeRooms, data);
    const actual = fakeRooms.querySelector(`[data-room=${data.currentRoom}`).dataset.unread;

    const expected = undefined;
    assert.equal(actual, expected);
  });
});

describe('hidePopup', () => {
  it('should return undefined if an argument is invalid', () => {
    const data = 'invalid';

    const actual = hidePopup(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should add class to node', () => {
    hidePopup(fakeInvitePopup);
    const actual = fakeInvitePopup.classList.contains('hidden');

    const expected = true;
    assert.equal(actual, expected);
  });
});

describe('showPopup', () => {
  it('should return undefined if an argument is invalid', () => {
    const data = 'invalid';

    const actual = showPopup(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should remove class from node', () => {
    showPopup(fakeInvitePopup);
    const actual = fakeInvitePopup.classList.contains('hidden');

    const expected = false;
    assert.equal(actual, expected);
  });
});

describe('showRoom', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should return undefined if the first argument is invalid', () => {
    const data = 99;

    const actual = showRoom(data);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should call createRoom', () => {
    const stub = sandbox.stub(window, 'createRoom'),
      data = {
        roomName: 'room',
        room: 'room'
      };
    
    showRoom(fakeRooms, data);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, data);
  })

  it('should insert DOM element in to container', () => {
    const data = {
      roomName: 'room',
      room: 'room'
    };

    showRoom(fakeRooms, data);
    const actual = fakeRooms.lastElementChild.outerHTML;

    const expected = '<div class="room" data-room="room">room<button class="btn btn--empty room__close-btn" data-leave="room"><i class="fas fa-sign-out-alt"></i></button></div>';
    assert.equal(actual, expected);
  });
})

mocha.run();