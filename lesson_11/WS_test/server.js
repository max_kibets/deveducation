const WebSockerServer = require('ws'),
  clients = {},
  webSocketServer = new WebSockerServer.Server({port: 8081});

webSocketServer.on('connection', (ws) => {
  const id = Math.random();

  clients[id] = ws;

  console.log('New conection', id);

  ws.on('message', (message) => {
    console.log('Got new message', message);

    for (const key in clients) {
      clients[key].send(message);
    };
  });
});

console.log('run!!!');