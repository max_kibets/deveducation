const socket = new WebSocket('ws://localhost:8081');

document.forms.publish.onsubmit = function(event) {
  event.preventDefault();
  const text = this.message.value;

  socket.send(text);
};

socket.onmessage = (event) => {
  const serverMessage = event.data;

  showMessage(serverMessage);
};

function showMessage(msg) {
  const messageContainer = document.getElementById('subscribe'),
    messageElement = document.createElement('div');

  messageElement.appendChild(document.createTextNode(msg));
  messageContainer.appendChild(messageElement);
}