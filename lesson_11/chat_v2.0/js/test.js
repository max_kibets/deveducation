// mocha.setup('bdd');

// const assert = chai.assert,
//   fakePopup = document.getElementById('popup').cloneNode(true),
//   fakePopupBtn = fakePopup.querySelector('button'),
//   fakeChat = document.getElementById('chat').cloneNode(true),
//   fakeTextarea = fakeChat.querySelector('textarea'),
//   fakeSendBtn = fakeChat.querySelector('button'),
//   fakeRoomBtns = fakeChat.querySelectorAll('[data-room]'),
//   fakeHistoryContainer = fakeChat.querySelector('#history'),
//   fakeRoomContainer = fakeChat.querySelector('#rooms');

// describe('onLogIn', () => {
//   let sandbox;

//   before(() => {
//     sandbox = sinon.createSandbox();
//   });
  
//   afterEach(() => {
//     sandbox.restore();
//   });

//   it('should call requestAuth() if popup input value', () => {
//     const stub = sandbox.stub(window, 'requestAuth'),
//       input = fakePopup.querySelector('input');
//     input.value = 'some data';

//     fakePopup.addEventListener('click', onLogIn);
//     fakePopupBtn.click();

//     sandbox.assert.calledOnce(stub);
// 		sandbox.assert.calledWith(stub, input.value);
//   });
// });

// describe('onMessage', () => {
//   let sandbox;

//   before(() => {
//     sandbox = sinon.createSandbox();
//   });
  
//   afterEach(() => {
//     sandbox.restore();
//   });

//   it('should call JSON.parse', () => {
//     const message = {
//         data: "{\"type\":\"authorize\"}"
//       },
//       stub = sandbox.stub(JSON, 'parse').returns({type: ''});

//     onMessage(message);

//     sandbox.assert.calledOnce(stub);
//     sandbox.assert.calledWith(stub, message.data);
//   });

// })

// describe('onChatClick', () => {
//   let sandbox;

//   before(() => {
//     sandbox = sinon.createSandbox();
//   });
  
//   afterEach(() => {
//     sandbox.restore();
//   });

//   it('should call sendMessage() if send button was pressed', () => {
//     const stub = sandbox.stub(window, 'sendMessage');

//     fakeChat.addEventListener('click', onChatClick);
//     fakeSendBtn.click();

//     sandbox.assert.calledOnce(stub);
// 		sandbox.assert.calledWith(stub, fakeTextarea);
//   });
// });

// describe('showMessage', () => {
//   let sandbox;

//   before(() => {
//     sandbox = sinon.createSandbox();
//   });
  
//   afterEach(() => {
//     sandbox.restore();
//   });

//   it('should call createMessage()', () => {
//     const stub = sandbox.stub(window, 'createMessage'),
//       user = {
//         name: '',
//         message: ''
//       };

//     showMessage(user);
  
//     sandbox.assert.calledOnce(stub);
//     sandbox.assert.calledWith(stub, user);
//   });
// })

// describe('createMessage', () => {
//   it('should return null if name is invalid', () => {
//     const user = {
//       message: 'some text'
//     };

//     const actual = createMessage(user);

//     const expected = null;
//     assert.equal(actual, expected);
//   });

//   it('should return null if message is invalid', () => {
//     const user = {
//       name: '1',
//     };

//     const actual = createMessage(user);

//     const expected = null;
//     assert.equal(actual, expected);
//   });
  
//   it('should return DOM element', () => {
//     const user = {
//       name: '1',
//       message: '1'
//     };

//     const actual = createMessage(user).outerHTML;

//     const expected = '<div class="msg"><div class="msg__name">1</div><div class="msg__text">1</div></div>';
//     assert.equal(actual, expected);
//   });
// });

// describe('showSubscribers', () => {
//   let sandbox;

//   before(() => {
//     sandbox = sinon.createSandbox();
//   });
  
//   afterEach(() => {
//     sandbox.restore();
//   });

//   it('should call createSubscriber()', () => {
//     const stub = sandbox.stub(window, 'createSubscriber'),
//       subs = {name: '3', status: 'online'}

//     showSubscribers(subs);
  
//     sandbox.assert.calledWith(stub, subs);
//   });
// });

// describe('createSubscriber', () => {
//   it('should return null if name is invalid', () => {
//     const user = {
//       status: 'online'
//     };

//     const actual = createSubscriber(user);

//     const expected = null;
//     assert.equal(actual, expected);
//   });

//   it('should return null if status is invalid', () => {
//     const user = {
//       name: '1',
//     };

//     const actual = createSubscriber(user);

//     const expected = null;
//     assert.equal(actual, expected);
//   });

//   it('should return DOM element', () => {
//     const user = {
//       name: '1',
//       status: 'online'
//     };

//     const actual = createSubscriber(user).outerHTML;

//     const expected = '<div class="user"><span class="online"></span> 1</div>';
//     assert.equal(actual, expected);
//   });
// });

// describe('hidePopup', () => {
//   let sandbox;

//   before(() => {
//     sandbox = sinon.createSandbox();
//   });
  
//   afterEach(() => {
//     sandbox.restore();
//   });

//   it('should return undefined if an argument isn\'t node', () => {
//     const actual = hidePopup('not a node');

//     const expected = undefined;
//     assert.equal(actual, expected);
//   });

//   it('should remove class "popup--hidden" from node', () => {
//     fakePopup.classList.remove('popup--hidden');

//     hidePopup(fakePopup);
//     const actual = fakePopup.classList.contains('popup--hidden');

//     const expected = true;
//     assert.equal(actual, expected);
//   });

//   it('should call removeEventListener()', () => {
//     const stub = sandbox.stub(fakePopup, 'removeEventListener');

//     hidePopup(fakePopup);

//     sandbox.assert.calledOnce(stub);
//     sandbox.assert.calledWith(stub, 'click', onLogIn);
//   });
// });

// describe('changeRoom', () => {
//   it('should return undefined if an argument isn\'t node', () => {
//     const actual = changeRoom('not a node');

//     const expected = undefined;
//     assert.equal(actual, expected);
//   });

//   // it('should add class "room--active" to node', () => {
//   //   changeRoom(fakeRoomBtns[0]);
//   //   const actual = fakeRoomBtns[0].classList.contains('room--active')

//   //   const expected = true;
//   //   assert.equal(actual, expected);
//   // });

//   // it('should remove class "room--active" from rest nodes', () => {
//   //   fakeRoomBtns[1].classList.add('room--active');

//   //   changeRoom(fakeRoomBtns[0]);
//   //   const actual = fakeRoomBtns[1].classList.contains('room--active')

//   //   const expected = false;
//   //   assert.equal(actual, expected);
//   // });
// });

// mocha.run();