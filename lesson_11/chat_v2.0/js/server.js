const WebSocker = require('ws'),
  webSocketServer = new WebSocker.Server({port: 8081}),
  users = [],
  history = {
    room_1: [],
    room_2: [],
    room_3: []
  }

webSocketServer.on('connection', (ws) => {
  ws.on('message', (message) => {
    const event = JSON.parse(message);

    if (event.type === 'authorize') {
      authHandler(ws, event.user);
    }

    if (event.type === 'change_room') {
      changeRoomHandler(event.user);
    }

    if (event.type === 'message') {
      messageHandler(event.user);
    }
  });

  ws.on('close', () => {
    const disconnectedUser = users.find(user => user.ws === ws);

    if (disconnectedUser) {
      disconnectedUser.status = 'offline';

      updateAllClientsSubscribers(disconnectedUser);
      
      console.log(`user ${disconnectedUser.name} ${disconnectedUser.status}`);
    }
  });
});

function messageHandler(incomingUser) {
  const registredUser = users.find(user => user.name === incomingUser.name);

  // subscribe if need
  if (!registredUser.subsribedRooms.includes(incomingUser.currentRoom)) {
    registredUser.subsribedRooms.push(incomingUser.currentRoom);

    updateClientHistory(registredUser);

    updateAllClientsSubscribers(registredUser);

    console.log(`user ${registredUser.name} subscribed to ${incomingUser.currentRoom}`);
  }

  //save history
  history[incomingUser.currentRoom].push(incomingUser);
  registredUser.message = incomingUser.message;

  // send message for all in room
  broadcastMessageForAll(registredUser);

  console.log(`user ${registredUser.name} sent a message`);
}

function changeRoomHandler(incomingUser) {
  const registredUser = users.find(user => user.name === incomingUser.name);

  registredUser.currentRoom = incomingUser.currentRoom;

  if (registredUser.subsribedRooms.includes(registredUser.currentRoom)) {
    // if need update room history
    updateClientHistory(registredUser);
  
    updateClientSubscribers(registredUser);
  }

  console.log(`user ${registredUser.name} changed room to ${registredUser.currentRoom}`);
}

function broadcastMessageForAll(incomingUser) {
  for (const user of users) {
    if (user.subsribedRooms.includes(incomingUser.currentRoom) && user.currentRoom === incomingUser.currentRoom) {
      broadcast('message', user.ws, incomingUser);
    }
  }
}

function updateClientHistory(incomingUser) {
  for (const historyUser of history[incomingUser.currentRoom]) {
    broadcast('message', incomingUser.ws, historyUser);
  }
}

function updateAllClientsSubscribers(incomingUser) {
  // for (let i = 0; i < users.length; i++) {
  //   if (users[i].subsribedRooms.includes(incomingUser.currentRoom)) {
  //     for (const user of users) {
  //       if (user.subsribedRooms.includes(incomingUser.currentRoom)) {
  //         updateClientSubscribers(user);
  //       }
  //     }
  //   }
  // }

  for (const room of incomingUser.subsribedRooms) {
    for (const user of users) {
      if (user.subsribedRooms.includes(room)) updateClientSubscribers(user);
    }
  }

  console.log(`sub updated!`);
}

function updateClientSubscribers(incomingUser) {
  for (let i = 0; i < users.length; i++) {
    if (users[i].subsribedRooms.includes(incomingUser.currentRoom)) {
      broadcast('update_sub', incomingUser.ws, (({ ws, ...rest }) => rest)(users[i]), i === 0);
    }
  }
}

function broadcast(type, ws, user, clear = false) {
  ws.send(JSON.stringify({
    type,
    clear,
    user
  }));
}

function authHandler(ws, incomingUser) {
  const registredUser = users.find(user => user.name === incomingUser.name);

  if (registredUser) {
    if (registredUser.status !== 'online') {
      registredUser.ws = ws;
      registredUser.status = 'online';

      updateAllClientsSubscribers(registredUser);

      ws.send(JSON.stringify({
        type: 'authorize',
        user: (({ ws, ...rest }) => rest)(registredUser)
      }));

      console.log(`user ${registredUser.name} ${registredUser.status}`);
    } else {
      console.log(`user ${registredUser.name} already ${registredUser.status}`);
    }
  }

  if (!registredUser) {
    incomingUser.ws = ws;
    incomingUser.status = 'online';
    incomingUser.subsribedRooms = [];
    users.push(incomingUser);

    ws.send(JSON.stringify({
      type: 'authorize',
      user: (({ ws, ...rest }) => rest)(incomingUser)
    }));

    console.log(`new user ${incomingUser.name} ${incomingUser.status}`);
  }
}