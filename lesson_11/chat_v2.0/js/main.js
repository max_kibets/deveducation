const socket = new WebSocket('ws://localhost:8081'),
  popup = document.getElementById('popup'),
  chat = document.getElementById('chat'),
  historyContainer = chat.querySelector('#history'),
  subscribersContainer = chat.querySelector('#subsrcibers');
  currentUser = {
    name: null,
    message: null
  };

popup.addEventListener('click', onLogIn);
socket.addEventListener('message', onMessage);
chat.addEventListener('click', onChatClick);

function onLogIn(event) {
  const authInput = this.querySelector('input');

  if (authInput.value && event.target.tagName === 'BUTTON') {
    requestAuth(authInput.value);
  }
}

function onMessage(message) {
  const event = JSON.parse(message.data);

  if (event.type === 'authorize') {
    currentUser = event.user;
    hidePopup(popup);
    subscribersContainer.innerHTML = '';
  }

  if (event.type === 'update_sub') {
    showSubscribers(event.user, event.clear);
  }

  if (event.type === 'message') {
    showMessage(event.user);
  }
}

function onChatClick(event) {
  if (event.target.tagName === 'BUTTON') {
    const textarea = this.querySelector('textarea');
    sendMessage(textarea);
  }

  if (event.target.dataset.room) {
    changeRoom(event.target);
    requestUpdateRoom(event.target);
  }
}

function requestAuth(login) {
  currentUser.name = login;

  socket.send(JSON.stringify({
    type: 'authorize',
    user: currentUser
  }));
}

function requestUpdateRoom(node) {
  socket.send(JSON.stringify({
    type: 'change_room',
    user: currentUser
  }));
}

function sendMessage(node) {
  if (!node.value) return void 0;

  if (currentUser.name && currentUser.currentRoom) {
    currentUser.message = node.value;

    socket.send(JSON.stringify({
      type: 'message',
      user: currentUser
    }));

    node.value = '';
  }
}

function showMessage(user) {
  const msg = createMessage(user);

  msg && historyContainer.appendChild(msg);

  scrollBottom(historyContainer.parentElement);
}

function createMessage({name, message}) {
  if (!name || !message) return null;

  const container = document.createElement('div');

  let current = currentUser.name === name ? " msg__name--current" : "";
  
  container.classList.add('msg');
  container.innerHTML = `<div class='msg__name${current}'>${name}</div><div class='msg__text'>${message}</div>`;

  return container;
}

function showSubscribers(subscriber, clear) {
  if (clear) subscribersContainer.innerHTML = '';

  const node = createSubscriber(subscriber);

  node && subscribersContainer.appendChild(node);
}

function createSubscriber({name, status}) {
  if (!name || !status) return null;

  const container = document.createElement('div');

  container.classList.add('user');
  container.innerHTML = `<span class="${status}"></span> ${name}`;

  return container;
}

function changeRoom(node) {
  if (!(node instanceof Node)) return void 0;

  const rooms = node.parentElement.children;

  Array.prototype.forEach.call(rooms, (room) => {
    room.classList.remove('room--active');
  });

  node.classList.add('room--active');
  currentUser.currentRoom = node.dataset.room;
  historyContainer.innerHTML = '';
  subscribersContainer.innerHTML = '';
}

function hidePopup(node) {
  if (!(node instanceof Node)) return void 0;

  node.classList.add('popup--hidden');
  node.removeEventListener('click', onLogIn);
}

function scrollBottom(elem) {
  elem.scrollTop = elem.scrollHeight;
}