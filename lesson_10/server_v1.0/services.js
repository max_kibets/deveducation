const mockHandler = require('./mock_handler');
  jsonHandler = require('./json_handler'),
  mysqlHandler = require('./mysql_handler'),
  sqliteHandler = require('./sqlite_handler'),
  psqlHandler = require('./psql_handler'),
  firebaseHandler = require('./firebase_handler');


exports.getUsers = async (res, db) => {
  let users = [];

  switch (db) {
    case 'mock':
      users = mockHandler.getItems();   
      break;

    case 'json':
      users = await jsonHandler.getItems();
      break;

    case 'mysql':
      users = await mysqlHandler.getItems();
      break;

    case 'sqlite':
      users = await sqliteHandler.getItems();
      break;

    case 'psql':
      users = await psqlHandler.getItems();
      break;

    case 'firebase':
      users = await firebaseHandler.getItems();
      break;
  }

  if (users.length) {
    res.writeHead(200, { 'Content-Type': 'text/json' });
    res.end(users);
  } else {
    res.writeHead(204, { 'Content-Type': 'text/plain' });
    res.end();
  }
};

exports.postUser = (req, res, db) => {
  let data; 

  req.on('data', chunk => {
    data = chunk;
  });

  req.on('end', async () => {
    let userID;

    switch (db) {
      case 'mock':
        userID = mockHandler.addItem(data);
        break;

      case 'json':
        userID = await jsonHandler.addItem(data);
        break;

      case 'mysql':
        userID = await mysqlHandler.addItem(data);
        break;

      case 'sqlite':
        userID = await sqliteHandler.addItem(data);
        break;

      case 'psql':
        userID = await psqlHandler.addItem(data);
        break;

      case 'firebase':
        userID = await firebaseHandler.addItem(data);
        break;
    }

    if (typeof userID === 'number') {
      res.writeHead(200, { 'Content-Type': 'text/plain' });  
    } else {
      res.writeHead(208, { 'Content-Type': 'text/plain' });
    }

    res.end(String(userID));
  });
};

exports.deleteUser = async (res, db, id) => {
  let isDeleted = false;

  switch (db) {
    case 'mock':
      isDeleted = mockHandler.deleteItem(id);
      break;

    case 'json':
      isDeleted = await jsonHandler.deleteItem(id);
      break;

    case 'mysql':
      isDeleted = await mysqlHandler.deleteItem(id);
      break;

    case 'sqlite':
      isDeleted = await sqliteHandler.deleteItem(id);
      break;

    case 'psql':
      isDeleted = await psqlHandler.deleteItem(id);
      break;

    case 'firebase':
      isDeleted = await firebaseHandler.deleteItem(id);
      break;
  }

  if (isDeleted) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('User have been deleted');
  } else {
    res.writeHead(204, { 'Content-Type': 'text/plain' });
    res.end('User already deleted');
  }
}

exports.putUser = (req, res, db, id) => {
  let data;
  
  req.on('data', chunk => {
    data = chunk;
  });
  
  req.on('end', async () => {
    let isEdited;

    switch (db) {
      case 'mock':
        isEdited = mockHandler.editItem(data);
        break;

      case 'json':
        isEdited = await jsonHandler.editItem(data);
        break;

      case 'mysql':
        isEdited = await mysqlHandler.editItem(data);
        break;

      case 'sqlite':
        isEdited = await sqliteHandler.editItem(data);
        break;

      case 'psql':
        isEdited = await psqlHandler.editItem(data);
        break;

      case 'firebase':
        isEdited = await firebaseHandler.editItem(data);
        break;
    }

    if (isEdited) {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('User have been updated');
    } else {
      res.writeHead(208, { 'Content-Type': 'text/plain' });
      res.end(`This user already exists`);
    }
  });
}