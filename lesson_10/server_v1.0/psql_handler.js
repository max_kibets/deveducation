const { Client } = require('pg')
  connectionString = `postgres://postgres:1111@localhost:5432/users`;

const psqlDB = new Client({
  connectionString: connectionString,
});

psqlDB.connect(error => {
  if (error) console.log(error)
  else console.log('postgres connected!');
})

function queryPsqlDB(sql) {
  return new Promise((resolve, reject) => {
    psqlDB.query(sql, (error, rows) => {
      if (error) return reject(error);
      resolve(JSON.stringify(rows));
    });
  }).catch(error => console.log(error));
};

exports.getItems = async () => {
  const answer = await queryPsqlDB('SELECT * FROM users');
  return JSON.stringify(JSON.parse(answer).rows);
};

exports.addItem = async (data) => {
  const maxId = await queryPsqlDB('SELECT MAX(id) FROM users');
  const user = JSON.parse(data);
  const {name, surname, age} = user;
  user.id = JSON.parse(maxId).rows[0].max + 1;
  await queryPsqlDB(`INSERT INTO users(id, name, surname, age) VALUES (${user.id}, '${name}', '${surname}', ${age})`);
  return user.id;
};

exports.deleteItem = (id) => {
  queryPsqlDB(`DELETE FROM users WHERE id=${id}`);

  return true;
};

exports.editItem = (data) => {
  const user = JSON.parse(data),
    {name, surname, age, id} = user;

  queryPsqlDB(`UPDATE users SET name = '${name}', surname = '${surname}', age=${age} WHERE id=${id}`);

  return true;
}