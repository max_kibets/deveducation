const hostname = 'localhost',
	port = 3000,
	server = require('./controller.js');

server.listen(port, hostname, () => {
	console.log(`server is running at http://${hostname}:${port}/`);
});