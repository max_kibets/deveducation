const form = document.getElementById('form'),
  table = document.getElementById('table'),
  tbody = table.querySelector('#table > tbody'),
  selectDB = document.getElementById('selectDB');

let db = 'mock';
 
window.addEventListener('load', loadHandler);
form.addEventListener('click', formClickHandler);
table.addEventListener('click', tableCLickHandler),
selectDB.addEventListener('change', onSelectChange);


// EVENT HANDLERS
function loadHandler() {
  parseUsers(tbody);
}

function formClickHandler(event) {
  event.preventDefault();

  if (event.target.dataset.btn === 'add') {
    addUser(tbody, form);
    toggleTrActiveClass();
    resetInputs(form);
  }

  if (event.target.dataset.btn === 'delete') {
    removeUser(tbody);
    resetInputs(form);
  }

  if (event.target.dataset.btn === 'update') {
    updateUser(tbody, form);
  }
}

function tableCLickHandler(event) {
  if (event.target.nodeName === 'TD') {
    const tr = event.target.parentElement;

    toggleTrActiveClass(tr);

    updateForm(Array.prototype.slice.apply(tr.children, [0, 3]));
  };
}

function onSelectChange(event) {
  db = event.target.value;
  tbody.innerHTML = '';
  parseUsers(tbody);
}

// REQUESTS
async function getUsers() {
  try {
    return await axios.get(`/users?db=${db}`);
  } catch (error) {
    console.log(error);
  }
}

async function postUser(data) {
  try {
    return await axios.post(`/users?db=${db}`, JSON.stringify(data));
  } catch (error) {
    console.log(error);
  }
}

async function deleteUser(id) {
  try {
    return await axios.delete(`/users?db=${db}&id=${id}`);
  } catch (error) {
    console.log(error);
  }
}

async function putUser(data, id) {
  try {
    return await axios.put(`/users?db=${db}&id=${id}`, JSON.stringify(data));
  } catch (error) {
    console.log(error);
  }
}

function resetInputs(form) {
  const inputs = form.querySelectorAll('.input');
  inputs.forEach(input => input.value = '');
}

function toggleTrActiveClass(tr) {
  const trs = table.querySelectorAll('tbody > tr');

  trs.forEach(item => {
    item.classList.remove('table__tr--active');
  });

  tr && tr.classList.add('table__tr--active');
}

function updateForm(rowData) {
  const inputs = form.querySelectorAll('input');
  
  Array.prototype.forEach.call(rowData, (item, i) => {
    inputs[i].value = item.textContent;
  })
}

async function parseUsers(container) {
  if (!container) return void 0;

  container.innerHTML = '';
  
  const answer = await getUsers();

  if (answer.status === 200) {
    for (const user of answer.data) {
      insertRow(container, user);
    }
  }
}

function insertRow(node, data, where = 'beforeend') {
  if (!node) return void 0;

  const tr = createRow(data);

  tr && node.insertAdjacentElement(where, tr);
}

function createRow(data) {
  if (!data) return void 0;

  const tr = document.createElement('tr');

  tr.id = data.id;
  tr.classList.add('table__tr');
  content = `<td class='table__td'>${data.name}</td><td class='table__td'>${data.surname}</td><td class='table__td'>${data.age}</td><td class='table__td'>${data.id}</td>`
  tr.innerHTML = content;

  return tr;
}

async function addUser(container, form) {
  if (!container || !form) return void 0;

  const fields = getFormData(form);

  if (fields) {
    const answer = await postUser(fields);

    fields.id = answer.data;
  
    if (answer.status === 200) {
      insertRow(container, fields);
    } else {
      alert('This user already added!');
    }
  }
}

async function removeUser(container) {
  if (!container) return void 0;

  const id = getUserID(container);

  if (id) {
    const answer = await deleteUser(id);
  
    if (answer.status === 200) {
      document.getElementById(id).remove();
    } else if (answer.status === 204) {
      alert('User already deleted!');
    }
  }
}

async function updateUser(container, form) {
  if (!container || !form) return void 0;

  const fields = getFormData(form),
    id = getUserID(container);

  if (fields && id) {
    fields.id = id;
    const answer = await putUser(fields);

    if (answer.status === 200) {
      const oldNode = document.getElementById(id);

      insertRow(oldNode, fields, 'afterend');
      oldNode.remove();
    } else {
      alert('This user already exists!');
    }
  }
}

function getFormData(form) {
  const formData = Object.fromEntries(new FormData(form));

  if (Object.values(formData).every(item => !!item)) {
    return formData;
  };

  return null;
}

function getUserID(container) {
  const elem = container.querySelector('.table__tr--active');

  if (elem) return elem.id;
}