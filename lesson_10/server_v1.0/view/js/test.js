mocha.setup('bdd');

const assert = chai.assert,
  fakeTbody = document.querySelector('#table > tbody').cloneNode(true),
  fakeForm = document.getElementById('form').cloneNode(true);

describe('loadHandler', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should call parseUsers()', () => {
    const stub = sandbox.stub(window, 'parseUsers');

    loadHandler();
    
    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, tbody);
  });
});

describe('formClickHandler', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  // it('should call event.preventDefault()', () => {
  //   const event = {
  //       preventDefault: () => {},
  //       target: {
  //         dataset: {
  //           btn: 'add'
  //         }
  //       }
  //     },
  //     stub = sandbox.stub(event, 'preventDefault');

  //   formClickHandler(event);

  //   sandbox.assert.calledOnce(stub);
  // });

  it('should call addUser()', async () => {
    const stub = sandbox.stub(window, 'addUser'),
      event = {
        preventDefault: () => {},
        target: {
          dataset: {
            btn: 'add'
          }
        }
      };

    await formClickHandler(event);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, tbody);
  });

  it('should call resetInputs()', async () => {
    const stub = sandbox.stub(window, 'resetInputs'),
      event = {
        preventDefault: () => {},
        target: {
          dataset: {
            btn: 'add'
          }
        }
      };

    await formClickHandler(event);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, form);
  });

  it('should call toggleTrActiveClass()', async () => {
    const stub = sandbox.stub(window, 'toggleTrActiveClass'),
      event = {
        preventDefault: () => {},
        target: {
          dataset: {
            btn: 'add'
          }
        }
      };

    await formClickHandler(event);

    sandbox.assert.calledOnce(stub);
  });

  it('should call removeUser()', async () => {
    const stub = sandbox.stub(window, 'removeUser'),
      event = {
        preventDefault: () => {},
        target: {
          dataset: {
            btn: 'delete'
          }
        }
      };

    await formClickHandler(event);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, tbody);
  });

  it('should call updateUser()', async () => {
    const stub = sandbox.stub(window, 'updateUser'),
      event = {
        preventDefault: () => {},
        target: {
          dataset: {
            btn: 'update'
          }
        }
      };

    await formClickHandler(event);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, tbody);
  });
});

describe('getUsers', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should call axios.get()', () => {
    const stub = sandbox.stub(axios, 'get'),
      url = '/users?db=mock';

    getUsers();

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, url);
  });

  it('should throw an error message', () => {
    const stub = sandbox.stub(console, 'log');
      error = new TypeError('Error message');
    sandbox.stub(axios, 'get').throws(error);

    getUsers();

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, error);
  });

  it('should return users data', async () => {
    let actual;

    await getUsers().then(response => {
      actual = !!response.status;
    });

    const expected = true;
    assert.equal(actual, expected);
  });
});

describe('parseUsers', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should return undefined if an argument is invalid', async () => {
    const actual = await parseUsers();

    const expected = undefined;
    assert.equal(actual, expected);
  })

  it('should call getUsers()', async () => {
    const answer = {
        status: 200,
        data: []
      },
      stub = sandbox.stub(window, 'getUsers').returns(answer),
      container = document.createElement('div');

    await parseUsers(container);

    sandbox.assert.calledOnce(stub);
  });

  it('should call insertRow()', async () => {
    const container = document.createElement('div'),
      answer = {
        status: 200,
        data: [{}]
      },
      stub = sandbox.stub(window, 'insertRow');
    sandbox.stub(window, 'getUsers').returns(answer);

    await parseUsers(container);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, container, answer.data[0]);
  });
});

describe('insertRow', () => {
  it('should return undefined if the first argument is invalid', () => {
    const actual = insertRow(null, []);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should insert data into container', () => {
    const data = [
        {name: 'name', lname: 'lname', age: 3},
        {name: 'name', lname: 'lname', age: 3}
      ],
      container = document.createElement('div');
    insertRow(container, data);

    const actual = container.outerHTML;

    const expected = '<div><tr id="undefined" class="table__tr"><td class="table__td">undefined</td><td class="table__td">undefined</td><td class="table__td">undefined</td><td class="table__td">undefined</td></tr></div>';
    assert.equal(actual, expected);
  });

  it('should insert data into container', () => {
    const data = 'string',
      container = document.createElement('div');
    insertRow(container, data);

    const actual = container.outerHTML;

    const expected = '<div><tr id="undefined" class="table__tr"><td class="table__td">undefined</td><td class="table__td">undefined</td><td class="table__td">undefined</td><td class="table__td">undefined</td></tr></div>';
    assert.equal(actual, expected);
  });
});

describe('createRow', () => {
  it('should return undefined if an argument is invalid', () => {
    const actual = createRow();

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return undefined if an argument is invalid', () => {
    const actual = createRow(null);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return DOM element', () => {
    const data = {
        name: 'name',
        lname: 'lname',
        age: 2
      };

    const actual = createRow(data).outerHTML;

    const expected = '<tr id="undefined" class="table__tr"><td class="table__td">name</td><td class="table__td">undefined</td><td class="table__td">2</td><td class="table__td">undefined</td></tr>';
    assert.equal(actual, expected);
  });

  it('should return DOM element', () => {
    const data = 'string';

    const actual = createRow(data).outerHTML;

    const expected = '<tr id="undefined" class="table__tr"><td class="table__td">undefined</td><td class="table__td">undefined</td><td class="table__td">undefined</td><td class="table__td">undefined</td></tr>';
    assert.equal(actual, expected);
  });
});

describe('addUser', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should return undefined if the first argument is invalid', async () => {
    const actual = await addUser(null, form);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return undefined if the second argument is invalid', async () => {
    const actual = await addUser(fakeTbody, null);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should call getFormData()', () => {
    const stub = sandbox.stub(window, 'getFormData');
    fakeTbody.innerHTML = '';

    addUser(fakeTbody, fakeForm);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, fakeForm);
  });

  it('should call postUser()', async () => {
    const answer = {
        status: 200,
        data: []
      },
      stub = sandbox.stub(window, 'postUser').returns(answer);
      sandbox.stub(window, 'getFormData').returns({ age: '1', name: '1', surname: '1' });

    await addUser(fakeTbody, fakeForm);

    sandbox.assert.calledOnce(stub);
  });

  it('should call insertRow()', async () => {
    const stub = sandbox.stub(window, 'insertRow'),
      fakeFormData = { age: '1', name: '1', surname: '1' }
    sandbox.stub(window, 'getFormData').returns(fakeFormData);
    sandbox.stub(axios, 'post').returns({status: 200});

    await addUser(fakeTbody, fakeForm);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, fakeTbody, fakeFormData);
  });
});

describe('postUser', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should call axios.post()', () => {
    const stub = sandbox.stub(axios, 'post'),
      url = '/users?db=mock',
      data = {};

      postUser(data);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, url, JSON.stringify(data));
  });

  it('should throw an error message', () => {
    const stub = sandbox.stub(console, 'log'),
      error = new TypeError('Error message'),
      data = {};
    sandbox.stub(axios, 'post').throws(error);

    postUser(data);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, error);
  });
});

describe('removeUser', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should return undefined if an argument is invalid', async () => {
    const actual = await removeUser();

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should call deleteUser', async () => {
    const stub = sandbox.stub(window, 'deleteUser').returns({stauts: 200}),
      tr = createRow({id: 1, name: '', surname: '', age: 0});
    sandbox.stub(window, 'getUserID').returns(1);
    tr.classList.add('table__tr--active');
    fakeTbody.innerHTML = '';
    fakeTbody.appendChild(tr);

    await removeUser(fakeTbody);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, 1);
  })
});

describe('deleteUser', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should call axios.delete()', () => {
    const stub = sandbox.stub(axios, 'delete').returns({stauts: 200}),
      url = '/users?db=mock&id=1';

    deleteUser(1);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, url);
  });

  it('should throw an error message', () => {
    const stub = sandbox.stub(console, 'log');
      error = new TypeError('Error message');
    sandbox.stub(axios, 'delete').throws(error);

    deleteUser(1);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, error);
  });
});

describe('updateUser', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should return undefined if the first argument is invalid', async () => {
    const actual = await updateUser(null, fakeForm);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should return undefined if the second argument is invalid', async () => {
    const actual = await updateUser(fakeTbody, null);

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('should call getFormData()', () => {
    const stub = sandbox.stub(window, 'getFormData');

    updateUser(fakeTbody, fakeForm);

    sandbox.assert.calledOnce(stub);
    sandbox.assert.calledWith(stub, fakeForm);
  });
})

mocha.run();