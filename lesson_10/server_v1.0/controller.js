const http = require('http'),
  filesHandler = require('./files_handler'),
  service = require('./services'),
  url = require('url');

module.exports = http.createServer((req, res) => {
  req.setEncoding('utf8');

  req.on('error', (err) => {
    console.error(err);
    res.statusCode = 400;
    res.end();
  });
  
  res.on('error', (err) => {
    console.error(err);
  });

  const parsedURL = url.parse(req.url, true);

  if (parsedURL.pathname === '/users') {
    switch (req.method) {
      case 'GET':
        service.getUsers(res, parsedURL.query.db);
        break;

      case 'POST':
        service.postUser(req, res, parsedURL.query.db);
        break;

      case 'DELETE':
        service.deleteUser(res, parsedURL.query.db, parsedURL.query.id);
        break;
        
      case 'PUT':
        service.putUser(req, res, parsedURL.query.db);
        break;
    }
  } else {
    filesHandler.getFilesRequest(req, res);
  }
});