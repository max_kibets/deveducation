const url = require('url'),
  path = require('path'),
  fs = require('fs');

exports.getFilesRequest = (req, res) => {
  const parsedURL = url.parse(req.url, true),
    pathname = (parsedURL.pathname === '/') ?  '/index.html': parsedURL.pathname,
    filePath = path.join(__dirname, 'view', pathname); 
  
  fs.readFile(filePath, (error, data) => {
    if (error) {
      res.writeHead(404, { 'Content-Type': 'text/plain' });
      res.end('Resourse not found!');
    } else {
      if (pathname.endsWith('.css')) {
        res.writeHead(200, { 'Content-Type': 'text/css' });
      }

      if (pathname.endsWith('.js')) {
        res.writeHead(200, { 'Content-Type': 'text/js' });
      }

      if (pathname.endsWith('.html')) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
      }

      res.end(data);
    }
  });
};