const mysql = require('mysql'),
  options = {
    user: 'root',
    password: '',
    database: 'hw_users',
  },
  mysqlDB = mysql.createConnection(options);

mysqlDB.connect(err => {
  if (err) {
    console.error('An error occurred while connecting to the DB')
    throw err
  }

  console.log('MySQL DB connected!');
});

function queryMysqlDB(sql, data) {
  return new Promise((resolve, reject) => {
    mysqlDB.query(sql, data, (error, rows) => {
      if (error) return reject(error);
      resolve(JSON.stringify(rows));
    });
  }).catch(error => console.log(error));
};

exports.getItems = () => {
  return queryMysqlDB('SELECT * FROM users');
};

exports.addItem = async (data) => {
  const maxId = await queryMysqlDB('SELECT MAX(id) FROM users');
  const user = JSON.parse(data);
  user.id = JSON.parse(maxId)[0]['MAX(id)'] + 1;
  await queryMysqlDB('INSERT INTO users SET ?', user);
  return user.id;
};

exports.deleteItem = (id) => {
  queryMysqlDB(`DELETE FROM users WHERE id=${id}`);

  return true;
};

exports.editItem = (data) => {
  const user = JSON.parse(data),
    {name, surname, age, id} = user;

  queryMysqlDB(`UPDATE users SET name="${name}", surname="${surname}", age=${age} WHERE id=${id}`);

  return true;
}