const fs = require('fs'),
  json = './db/users.json',
  charset = 'utf-8';

exports.getItems = () => {
  return new Promise((resolve, reject)=> {
    fs.readFile(json, charset, (error, data) => {
      if (error) {
        console.log("Error reading file from disk:", err)
        return
      }
  
      try {
        resolve(data);
      } catch(error) {
        console.log('Error parsing JSON string:', error)
      }
    });
  });
}

exports.addItem = (data) => {
  return new Promise((resolve, reject) => {
    fs.readFile(json, charset, (error, buffer) => {
      const user = JSON.parse(data);

      let maxID,
        users = JSON.parse(buffer);

      if (users.length) {
        const lastUser = users.reduce((prev, curr) => {
          return +curr.id > +prev.id ?  curr : prev;
        });
    
        maxID = +lastUser.id + 1;
      } else {
        maxID = 1;
      }
    
      user.id = maxID;
      users.push(user);
      fs.writeFileSync(json, JSON.stringify(users));

      try {
        resolve(user.id);
      } catch(error) {
        console.log('Error parsing JSON string:', error)
      }
    });
  });
}

exports.deleteItem = (id) => {
  return new Promise((resolve, reject) => {
    fs.readFile(json, charset, (error, buffer) => {
      let users = JSON.parse(buffer);

      users = users.filter(user => +user.id != +id);

      fs.writeFileSync(json, JSON.stringify(users));

      try {
        resolve(true);
      } catch(error) {
        console.log('Error parsing JSON string:', error)
      }
    });
  });
}

exports.editItem = (data) => {
  return new Promise((resolve, reject) => {
    fs.readFile(json, charset, (error, buffer) => {
      const editedUser = JSON.parse(data);

      let users = JSON.parse(buffer);

      users = users.map(user => {
        return (+user.id == +editedUser.id) ? editedUser : user;
      });

      fs.writeFileSync(json, JSON.stringify(users));

      try {
        resolve(true);
      } catch(error) {
        console.log('Error parsing JSON string:', error)
      }
    });
  });
}