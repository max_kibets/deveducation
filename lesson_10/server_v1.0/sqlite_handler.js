const sqlite3 = require('sqlite3').verbose();
 
// open the database
let sqliteDB = new sqlite3.Database('./db/sqlite.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the sqlite database.');
});

function querySqliteDB(sql, data) {
  return new Promise((resolve, reject) => {
    sqliteDB.all(sql, data, (error, rows) => {
      if (error) return reject(error);

      resolve(JSON.stringify(rows));
    })
  }).catch(error => console.log(error));
};

exports.getItems = async () => {
  return querySqliteDB('SELECT * FROM users');
};

exports.addItem = async (data) => {
  const maxId = await querySqliteDB('SELECT MAX(id) FROM users'),
    user = JSON.parse(data);
  user.id = JSON.parse(maxId)[0]['MAX(id)'] + 1;

  const keys = Object.keys(user).join(', ');
  
  await querySqliteDB(`INSERT INTO users (${keys}) VALUES (?, ?, ?, ?)`, Object.values(user));
  return user.id;
};

exports.deleteItem = (id) => {
  querySqliteDB(`DELETE FROM users WHERE id=${id}`);

  return true;
};

exports.editItem = (data) => {
  const user = JSON.parse(data),
    {name, surname, age, id} = user;

  console.log(`UPDATE users SET name=${name}, surname=${surname}, age=${age} WHERE id=${id}`)

  querySqliteDB(`UPDATE users SET name="${name}", surname="${surname}", age=${age} WHERE id=${id}`);

  return true;
}