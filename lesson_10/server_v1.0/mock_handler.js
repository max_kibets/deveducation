const mockDB = require('./db/mock');

exports.getItems = () => {
  return JSON.stringify(mockDB.users);
};

exports.addItem = (data) => {
  let maxID;

  if (mockDB.users.length) {
    const lastUser = mockDB.users.reduce((prev, curr) => {
      return +curr.id > +prev.id ?  curr : prev;
    });

    maxID = +lastUser.id + 1;
  } else {
    maxID = 1;
  }

  const user = JSON.parse(data);
    
  user.id = maxID;
  mockDB.users.push(user);
  
  return user.id;
};

exports.deleteItem = (id) => {
  mockDB.users = mockDB.users.filter(user => +user.id != +id);
  return true;
};

exports.editItem = (data) => {
  const editedUser = JSON.parse(data);

  mockDB.users = mockDB.users.map(user => {
    return (+user.id == +editedUser.id) ? editedUser : user;
  });

  return true;
}