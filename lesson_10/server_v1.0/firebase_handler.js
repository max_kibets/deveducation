const admin = require("firebase-admin");

const serviceAccount = require("./hw-users-firebase-adminsdk-8cc8a-d633067109.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hw-users.firebaseio.com"
});

const firebaseDB = admin.firestore();

const getItems = async () => {
  const data = await firebaseDB.collection('users').get(),
    users = [];

  data.forEach(doc => {
    users.push(doc.data());
  })

  return JSON.stringify(users);
};

const addItem = async (data) => {
  let users = JSON.parse(await getItems()),
    maxID;

  console.log(users);
  if (users.length) {
    const lastUser = users.reduce((prev, curr) => {
      return +curr.id > +prev.id ?  curr : prev;
    });

    maxID = +lastUser.id + 1;
  } else {
    maxID = 1;
  }


  const user = JSON.parse(data);

  user.id = maxID;

  await firebaseDB.collection('users').doc(`${user.id}`).set(user);

  return user.id;
}

const deleteItem = async (id) => {
  const answer = await firebaseDB.collection('users').doc(`${id}`).delete();
  return answer;
}

const editItem = async (data) => {
  const user = JSON.parse(data);
  const answer = await firebaseDB.collection('users').doc(`${user.id}`).update(user);
  return answer;
}

module.exports = {
  getItems,
  addItem,
  deleteItem,
  editItem
}