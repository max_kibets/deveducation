const form = document.getElementById('form'),
  table = document.getElementById('table'),
  popup = document.querySelector('.popup');

let dataList = [];

form.addEventListener('click', onFormClick);
table.addEventListener('click', onTableClick);
popup.addEventListener('click', hidePopup);

function onFormClick(event) {
  const btn = event.target.dataset.btn;
  let data;

  switch (btn) {
    case 'add-start':
    case 'add-middle':
    case 'add-end':
      data = collectData(form);
      udpateDataList(btn, data);
      break;
    case 'clear':
      clearDataList();
      break;
    case 'update':
      data = collectData(form);
      updateDataListItem(dataList, data);
      break;
    case 'delete':
      data = collectData(form);
      deleteDataListItem(dataList, data);
      break;
    case 'save':
      saveDataList(dataList);
      break;
    case 'read':
      loadStoredData();
      break;
  }

  if (btn) table.replaceChild(renderDataList(dataList), table.querySelector('tbody'));

  if (data && btn && btn !== 'save' && btn !== 'read') form.reset();
}

function onTableClick(event) {
  if (event.target.nodeName === 'TD') {
    const tr = event.target.parentElement;

    toggleTrActiveClass(tr);
    updateForm(tr.children);
  };
}

function toggleTrActiveClass(tr) {
  const trs = table.querySelectorAll('tbody > tr');

  trs.forEach(item => {
    item.classList.remove('table__tr--active');
  })

  tr && tr.classList.add('table__tr--active');
}

function updateForm(rowData) {
  const inputs = form.querySelectorAll('input');
  
  Array.prototype.forEach.call(rowData, (item, i) => {
    inputs[i].value = item.textContent;
  })
}

function udpateDataList(btn, data) {
  if (data) {
    const isId = isIdExists(dataList, data.id);
  
    if (!isId) {
      switch (btn) {
        case 'add-end':
          addDataEnd(dataList, data);
          break;
        case 'add-middle':
          addDataMiddle(dataList, data);
          break;
        case 'add-start':
          addDataStart(dataList, data);
          break;
      };
    } else {
      popup.classList.add('popup__active');
    };
  }
}

function collectData(form) {
  const inputs = form.querySelectorAll('input');

  if (inputs[0].value) {
    return {
      id: inputs[0].value,
      name: inputs[1].value,
      lastName: inputs[2].value,
      age: inputs[3].value
    };
  }

  return null;
}

function isIdExists(storage, id) {
  for (const item of storage) {
    if (item.id == id) return true;
  }

  return false;
}

function addDataEnd(storage, data) {
  data && storage.push(data);
}

function addDataStart(storage, data) {
  data && storage.unshift(data);
}

function addDataMiddle(storage, data) {
  if (data) {
    const pos = (storage.length / 2) < 1 ? 1 : Math.round(storage.length / 2);
    storage.splice(pos, 0, data);
  }
}

function clearDataList() {
  dataList = [];
}

function renderDataList(storage) {
  const container = document.createElement('tbody');

  if (storage.length) {
    for (const item of storage) {
      const tr = createRow(item);
  
      container.appendChild(tr);
    }
  }
  
  return container;
}

function createRow({id, name, lastName, age}) {
  const tr = document.createElement('tr');
  tr.classList.add('table__tr');
  
  tr.innerHTML = `<td class="table__td">${id}</td><td class="table__td">${name}</td><td class="table__td">${lastName}</td><td class="table__td">${age}</td>`;
  
  return tr;
}

function updateDataListItem(storage, data) {
  if (isIdExists(storage, data.id)) {
    storage.forEach((item, i, arr) => {
      if (item.id === data.id) arr[i] = data;
    });

    toggleTrActiveClass();
  }
}

function deleteDataListItem(storage, data) {
  if (data && isIdExists(storage, data.id)) {
    const index = storage.findIndex(item => item.id === data.id);
    console.log(index);
    storage.splice(index, 1);
  };
}

function saveDataList(storage) {
    localStorage.setItem('dataList', JSON.stringify(storage));
}

function loadStoredData() {
  const storedDataList = localStorage.getItem('dataList');

  if (storedDataList) dataList = JSON.parse(storedDataList);
}

function hidePopup(event) {
  if (event.target.dataset.btn) popup.classList.remove('popup__active');
}