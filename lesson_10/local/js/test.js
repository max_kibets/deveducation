mocha.setup('bdd');

const assert = chai.assert,
  btnAddEnd = document.querySelector('[data-btn=add-end]'),
  btnAddStart = document.querySelector('[data-btn=add-start]'),
  btnAddMiddle = document.querySelector('[data-btn=add-middle]'),
  btnClear = document.querySelector('[data-btn=clear]'),
  fakeForm = document.createElement('form'),
  inputs = `<input type="number" value="1">
            <input type="text" value="name">
            <input type="text" value="lname">
            <input type="number" value="33">`,
  fakeData = {
    id: 1,
    name: 'name',
    lastName: 'lname',
    age: 44
  };

let sandbox,
  fakeDataList = [];

before(() => {
  sandbox = sinon.createSandbox();
});

afterEach(() => {
  sandbox.restore();
});

describe('onFormClick', () => {
  it('should call collectData if click on btnAddEnd', () => {
    const stub = sandbox.stub(window, 'collectData');

    btnAddEnd.click();

    sandbox.assert.calledOnce(stub);
  });

  it('should call udpateDataList if click on btnAddEnd', () => {
    const stub = sandbox.stub(window, 'udpateDataList')

    btnAddEnd.click();

    sandbox.assert.calledOnce(stub);
  });

  it('should call collectData if click on btnAddStart', () => {
    const stub = sandbox.stub(window, 'collectData');

    btnAddStart.click();

    sandbox.assert.calledOnce(stub);
  });

  it('should call udpateDataList if click on btnAddStart', () => {
    const stub = sandbox.stub(window, 'udpateDataList')

    btnAddStart.click();

    sandbox.assert.calledOnce(stub);
  });

  it('should call collectData if click on btnAddMiddle', () => {
    const stub = sandbox.stub(window, 'collectData');

    btnAddMiddle.click();

    sandbox.assert.calledOnce(stub);
  });

  it('should call udpateDataList if click on btnAddMiddle', () => {
    const stub = sandbox.stub(window, 'udpateDataList')

    btnAddMiddle.click();

    sandbox.assert.calledOnce(stub);
  });

  it('should call clearDataList if click on btnClear', () => {
    const stub = sandbox.stub(window, 'clearDataList')

    btnClear.click();

    sandbox.assert.calledOnce(stub);
  });
});

describe('udpateDataList', () => {

});

describe('collectData', () => {
  it('should return null if all inputs are empty', () => {
    const actual = collectData(form);

    const expected = null;
    assert.equal(actual, expected);
  });

  it('should return an object with data from inputs', () => {
    fakeForm.innerHTML = inputs;

    const actual = collectData(fakeForm);

    const expected = {id: '1', name: 'name', lastName: 'lname', age: '33'};
    assert.deepEqual(actual, expected);
  });
});

describe('isIdExists', () => {
  it('should return true if an ID exists in storage', function() {
    const id = 1;
    fakeDataList.push(fakeData);

    const actual = isIdExists(fakeDataList, id);

    const expected = true;
    assert.equal(actual, expected);
  })
});

describe('addDataEnd', () => {
  it('fakeDataList length should equal 1', () => {
    fakeDataList = [];
    addDataEnd(fakeDataList, fakeData);
    const actual = fakeDataList.length;

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('addDataStart', () => {
  it('fakeDataList length should equal 1', () => {
    fakeDataList = [];
    addDataStart(fakeDataList, fakeData);
    const actual = fakeDataList.length;

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('addDataMiddle', () => {
  it('fakeDataList length should equal 1', () => {
    fakeDataList = [];
    addDataMiddle(fakeDataList, fakeData);
    const actual = fakeDataList.length;

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('renderDataList', () => {
  it('should return empty DOM element if storage is empty', () => {
    fakeDataList = [];

    const actual = renderDataList(fakeDataList);

    const expected = document.createElement('tbody');
    assert.deepEqual(actual, expected);
  });

  it('should return DOM element', () => {
    fakeDataList.push(fakeData);

    const actual = renderDataList(fakeDataList);

    const tbody = document.createElement('tbody');
    for (const item of fakeDataList) {
      const tr = createRow(item);
      tbody.appendChild(tr);
    }
    const expected = tbody;
    assert.deepEqual(actual, expected);
  });
})

// describe('createRow', () => {
//   it('should return DOM element', () => {
//     const actual = createRow(fakeData);

//     const tr = document.createElement('tr');
//     tr.classList.add('table__tr');
//     tr.innerHTML = `<td class="table__td">1</td><td class="table__td">name</td><td class="table__td">lname</td><td class="table__td">44</td>`;
//     const expected = tr;
//     assert.equal(actual, expected);
//   });
// });

describe('updateDataListItem', () => {
  it('should update dataList item', () => {
    const newFakeData = {
      id: 1,
      name: 'Adilf',
      lastName: 'Hitler',
      age: 56
    }
    fakeDataList.push(fakeData);

    updateDataListItem(fakeDataList, newFakeData);
    const actual = fakeDataList[0];

    const expected = newFakeData;
    assert.deepEqual(actual, expected);
  });
});

describe('deleteDataListItem', () => {
  fakeDataList.push(fakeData);

  deleteDataListItem(fakeDataList, fakeData);
  const actual = fakeDataList.length;

  const expected = 0;
  assert.equal(actual, expected);
});

mocha.run();