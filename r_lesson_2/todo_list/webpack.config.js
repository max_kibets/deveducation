const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  entry: {
    app: "./src/index.js"
  },
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "[name].[hash:8].js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                    forceAllTransforms: false,
                    modules: false,
                    useBuiltIns: false,
                    debug: false
                  }
                ],
                [
                  "@babel/preset-react",
                  {
                    development: true
                  }
                ]
              ],
              plugins: [
                ["@babel/plugin-proposal-class-properties", { loose: false }],
                "@babel/plugin-transform-spread",
                "@babel/plugin-proposal-export-default-from",
                ["@babel/plugin-proposal-decorators", { legacy: true }]
              ],
              babelrc: false
            }
          }
        ]
      },
      {
        test: /\.(less|css)$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "less-loader"]
      }
    ]
  },
  devtool: "#source-map",
  devServer: {
    overlay: true
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin({
      __DEV__: true
    }),
    new MiniCssExtractPlugin({
      filename: "[name].[hash:8].css"
    }),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      minify: {
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
        removeComments: true,
        useShortDoctype: true,
        keepClosingSlash: true,
        collapseWhitespace: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    })
  ],
  optimization: {
    minimize: false,
    noEmitOnErrors: false
  }
};
