function createTodoItem(label, id) {
  return {
    label,
    id,
    edit: false,
    done: false,
  };
}

function getMaxID(array) {
  return array.length ? Math.max(...array.map(item => item.id)) : 0;
}

function togglePropetry(array, id, prop) {
  return array.map(item => item.id === id ? {...item, [prop]: !item[prop]} : {...item})
}

export {
  createTodoItem,
  getMaxID,
  togglePropetry
};