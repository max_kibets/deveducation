import React, { useEffect } from 'react';

import './TodoListItemForm.less';

import Btn from '../Btn/';

const TodoListItemFrom = ({label, toggleEdit, onEdit, id}) => {
  let textInput = null;

  useEffect(()=>{
    textInput.focus();
  })

  function handlerEdit(event) {
    onEdit(id, event.target.value);
  }

  function handlerSubmit(event) {
    event.preventDefault();
    toggleEdit(id);
  }

  return (
    <form onSubmit={handlerSubmit} className='todo__container'>
      <input type="text" className='todo__input' value={label} onChange={handlerEdit} ref={input => textInput = input}/>
      <Btn text='OK' cssClasses='btn'/>
    </form>
  )
};

export default TodoListItemFrom;