import React, {Component} from 'react';

import './AddForm.less';

import Btn from '../Btn/';

export default class AddForm extends Component {
  state = {
    inputValue: ''
  };

  onValueChange = (event) => {
    this.setState({
      inputValue: event.target.value
    })
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.state.inputValue && this.props.onAddTodoItem(this.state.inputValue);

    this.setState({
      inputValue: ''
    })
  }

  render() {
    const {inputValue} = this.state;

    return (
      <form className='todo__form' onSubmit={this.onSubmit}>
        <input type="text" 
          className='input'
          onChange={this.onValueChange}
          value={inputValue}
          placeholder='type your ToDo...'
        />
        <Btn text='Add' cssClasses='btn'/>
      </form>
    )
  }
}

 AddForm;