import React from 'react';

import './TodoListItem.less';

import Btn from '../Btn/';

const TodoListItem = ({label, toggleEdit, toggleDone, done, onDelete}) => {
  return (
    <div className='todo__container'>
      <div onClick={toggleDone}
        className={`todo__text ${done ? 'todo__text--done' : ''}`}
      >{label}</div>

      {!done && <Btn handlerClick={toggleEdit} text='Edit' cssClasses='btn'/>}
      
      <Btn handlerClick={onDelete} text='X' cssClasses='btn btn--red'/>
    </div>
  )
};

export default TodoListItem;