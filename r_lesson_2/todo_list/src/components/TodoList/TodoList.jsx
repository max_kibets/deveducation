import React from 'react';

import './TodoList.less';

import TodoListItem from '../TodoListItem/';
import TodoListItemForm from '../TodoListItemForm/';

const TodoList = ({toggleEdit, toggleDone, onDelete, onEdit, todos}) => {
  const elements = todos.map(({label, id, isEdit, done}) => {
    return (
      <li key={`todo_item_${id}`} className='todo__list-item'>
        {isEdit ? (
          <TodoListItemForm
            toggleEdit={toggleEdit}
            onEdit={onEdit}
            label={label}
            id={id}
          />
        ) : (
          <TodoListItem
            toggleEdit={() => toggleEdit(id)}
            onDelete={() => onDelete(id)}
            toggleDone={() => toggleDone(id)}
            label={label}
            done={done}
          />
        )}
      </li>
    );
  });

  return (
    <ul className='todo__list'>
      {elements}
    </ul>
  );
};

export default TodoList;