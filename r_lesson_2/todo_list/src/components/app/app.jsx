import React, {Component} from 'react';

import './App.less';

import AddForm from '../AddForm/';
import TodoList from '../TodoList/';
import { createTodoItem, togglePropetry, getMaxID } from '../../helpers/logic';

export default class App extends Component {
  state = {
    todos: []
  };

  addTodoItem = (text) => {
    const id = getMaxID(this.state.todos) + 1;
    const todo = createTodoItem(text, id);

    this.setState(({todos}) => {
      const newTodos = [todo, ...todos]

      return {
        todos: newTodos
      }
    });
  };

  deleteTodoItem = (id) => {
    this.setState(({todos}) => {
      return {
        todos: todos.filter(item => item.id !== id)
      }
    })
  };

  toggleEdit = (id) => {
    this.setState(({todos}) => {
      return {
        todos: togglePropetry(todos, id, 'isEdit')
      }
    })
  };

  toggleDone = (id) => {
    this.setState(({todos}) => {
      return {
        todos: togglePropetry(todos, id, 'done')
      }
    })
  }

  updateTodoLabel = (id, label) => {
    this.setState(({todos}) => {
      return {
        todos: todos.map(todo => todo.id === id ? {...todo, label} : {...todo})
      }
    })
  }

  render() {
    return (
      <div className='todo'>
        <AddForm onAddTodoItem={this.addTodoItem}/>
        <TodoList
          toggleEdit={this.toggleEdit}
          onDelete={this.deleteTodoItem}
          onEdit={this.updateTodoLabel}
          toggleDone={this.toggleDone}
          {...this.state}
        />
      </div>
    )
  }
}