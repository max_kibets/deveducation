import React from 'react';

import './Btn.less';

const Btn = ({text, handlerClick, cssClasses}) => {
  return (
    <button onClick={handlerClick} className={cssClasses}>{text}</button>
  )
}

export default Btn;