mocha.setup('bdd');

const assert = chai.assert;

// ARRAY LIST 
describe('Alist', function() {
    describe('push', function() {
        it('should return 0 if an argument is empty', function() {
            const alist = new Alist();
    
            const actual = alist.push();
    
            const expected = 0;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [] if an argument is empty and Alist.root is empty', function() {
            const alist = new Alist();
    
            alist.push();
            const actual = alist.root;
    
            const expected = [];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 1 if an argument is 22', function() {
            const data = 22,
                alist = new Alist();
    
            const actual = alist.push(data);
    
            const expected = 1;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [22] if an argument is 22 and Alist.root is empty', function() {
            const data = 22,
                alist = new Alist();
    
            alist.push(data);
            const actual = alist.root;
    
            const expected = [22];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 2 if arguments are (11, 22)', function() {
            const data1 = 11,
                data2 = 22,
                alist = new Alist();
    
            const actual = alist.push(data1, data2);
    
            const expected = 2;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [11, 22] if arguments are (11, 22) and Alist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                alist = new Alist();
    
            alist.push(data1, data2);
            const actual = alist.root;
    
            const expected = [11, 22];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 2 if an argument is 22 and Alist.root is [11]', function() {
            const data = 22,
                alist = new Alist();
            alist.root = [11];
    
            const actual = alist.push(data);
    
            const expected = 2;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [11, 22] if an argument is (22) and Alist.root is [11]', function() {
            const data = 22,
                alist = new Alist();
            alist.root = [11];
    
            alist.push(data);
            const actual = alist.root;
    
            const expected = [11, 22];
            assert.deepEqual(actual, expected);
        });
    });
    
    describe('pop', function() {
        it('should return undefined if Alist.root is empty', function() {
            const alist = new Alist();
    
            const actual = alist.pop();
    
            const expected = undefined;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [] if Alist.root is empty', function() {
            const alist = new Alist();
    
            alist.pop();
            const actual = alist.root;
    
            const expected = [];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 11 if Alist.root is [11]', function() {
            const alist = new Alist();
            alist.root = [11];
    
            const actual = alist.pop();
    
            const expected = 11;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [] if Alist.root is [11]', function() {
            const alist = new Alist();
            alist.root = [11];
    
            alist.pop();
            const actual = alist.root;
    
            const expected = [];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 22 if Alist.root is [11, 22]', function() {
            const alist = new Alist();
            alist.root = [11, 22];
    
            const actual = alist.pop();
    
            const expected = 22;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [11] if Alist.root is [11, 22]', function() {
            const alist = new Alist();
            alist.root = [11, 22];
    
            alist.pop();
            const actual = alist.root;
    
            const expected = [11];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 55 if Alist.root is [11, 22, 33, 44, 55]', function() {
            const alist = new Alist();
            alist.root = [11, 22, 33, 44, 55];
    
            const actual = alist.pop();
    
            const expected = 55;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [11, 22, 33, 44] if Alist.root is [11, 22, 33, 44, 55]', function() {
            const alist = new Alist();
            alist.root = [11, 22, 33, 44, 55];
    
            alist.pop();
            const actual = alist.root;
    
            const expected = [11, 22, 33, 44];
            assert.deepEqual(actual, expected);
        });
    });
    
    describe('shift', function() {
        it('should return undefined if Alist.root is empty', function() {
            const alist = new Alist();
    
            const actual = alist.shift();
    
            const expected = undefined;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [] if Alist.root is empty', function() {
            const alist = new Alist();
    
            alist.shift();
            const actual = alist.root;
    
            const expected = [];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 11 if Alist.root is [11]', function() {
            const alist = new Alist();
            alist.root = [11];
    
            const actual = alist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [] if Alist.root is [11]', function() {
            const alist = new Alist();
            alist.root = [11];
    
            alist.shift();
            const actual = alist.root;
    
            const expected = [];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 11 if Alist.root is [11, 22]', function() {
            const alist = new Alist();
            alist.root = [11, 22];
    
            const actual = alist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [22] if Alist.root is [11, 22]', function() {
            const alist = new Alist();
            alist.root = [11, 22];
    
            alist.shift();
            const actual = alist.root;
    
            const expected = [22];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 11 if Alist.root is [11, 22, 33, 44, 55]', function() {
            const alist = new Alist();
            alist.root = [11, 22, 33, 44, 55];
    
            const actual = alist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [22, 33, 44, 55] if Alist.root is [11, 22, 33, 44, 55]', function() {
            const alist = new Alist();
            alist.root = [11, 22, 33, 44, 55];
    
            alist.shift();
            const actual = alist.root;
    
            const expected = [22, 33, 44, 55];
            assert.deepEqual(actual, expected);
        });
    });
    
    describe('unshift', function() {
        it('should return 0 if an argument is empty', function() {
            const alist = new Alist();
    
            const actual = alist.unshift();
    
            const expected = 0;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [] if an argument is empty and Alist.root is empty', function() {
            const alist = new Alist();
    
            alist.unshift();
            const actual = alist.root;
    
            const expected = [];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 1 if an argument is 22', function() {
            const data = 22,
                alist = new Alist();
    
            const actual = alist.unshift(data);
    
            const expected = 1;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [22] if an argument is 22 and Alist.root is empty', function() {
            const data = 22,
                alist = new Alist();
    
            alist.unshift(data);
            const actual = alist.root;
    
            const expected = [22];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 2 if arguments are (11, 22)', function() {
            const data1 = 11,
                data2 = 22,
                alist = new Alist();
    
            const actual = alist.unshift(data1, data2);
    
            const expected = 2;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [11, 22] if arguments are (11, 22) and Alist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                alist = new Alist();
    
            alist.unshift(data1, data2);
            const actual = alist.root;
    
            const expected = [11, 22];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 2 if an argument is 22 and Alist.root is [11]', function() {
            const data = 22,
                alist = new Alist();
            alist.root = [11];
    
            const actual = alist.unshift(data);
    
            const expected = 2;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [22, 11] if arguments are (22) and Alist.root is [11]', function() {
            const data = 22,
                alist = new Alist();
            alist.root = [11];
    
            alist.unshift(data);
            const actual = alist.root;
    
            const expected = [22, 11];
            assert.deepEqual(actual, expected);
        });
    
        it('should return 4 if an argument is (33,44) and Alist.root is [11,22]', function() {
            const data1 = 33,
                data2 = 44,
                alist = new Alist();
            alist.root = [11, 22];
    
            const actual = alist.unshift(data1, data2);
    
            const expected = 4;
            assert.equal(actual, expected);
        });
    
        it('Alist.root should equal [33, 44, 11, 22] if arguments are (33, 44) and Alist.root is [11, 22]', function() {
            const data1 = 33,
                data2 = 44,
                alist = new Alist();
            alist.root = [11, 22];
    
            alist.unshift(data1, data2);
            const actual = alist.root;
    
            const expected = [33, 44, 11, 22];
            assert.deepEqual(actual, expected);
        });
    });
    
    describe('toString', function() {
        it('should return an empty string if Alist.root is epmty', function() {
            const alist = new Alist();
    
            const actual = alist.toString();
    
            const expected = '';
            assert.equal(actual, expected);
        });
    
        it('should return "11" if Alist.root is [11]', function() {
            const alist = new Alist();
            alist.root = [11];
    
            const actual = alist.toString();
    
            const expected = '11';
            assert.equal(actual, expected);
        });
    
        it('should return "11, 22" if Alist.root is [11, 22]', function() {
            const alist = new Alist();
            alist.root = [11, 22];
    
            const actual = alist.toString();
    
            const expected = '11, 22';
            assert.equal(actual, expected);
        });
    
        it('should return "11, 22, 33, 44, 55" if Alist.root is [11, 22, 33, 44, 55]', function() {
            const alist = new Alist();
            alist.root = [11, 22, 33, 44, 55];
    
            const actual = alist.toString();
    
            const expected = '11, 22, 33, 44, 55';
            assert.equal(actual, expected);
        });
    });

    describe('map', function() {
        it('should return [] if an argument is ((item) => item * 2) and Alist.root is []', function() {
            const alist = new Alist(),
                data = (item) => item * 2;

            const actual = alist.map(data);

            const expected = [];
            assert.deepEqual(actual, expected);
        });

        it('should return [22] if an argument is ((item) => item * 2) and Alist.root is [11]', function() {
            const alist = new Alist(),
                data = (item) => item * 2;
            alist.root = [11];

            const actual = alist.map(data);

            const expected = [22];
            assert.deepEqual(actual, expected);
        });

        it('should return [22, 44, 66] if an argument is ((item) => item * 2) and Alist.root is [11, 22, 33]', function() {
            const alist = new Alist(),
                data = (item) => item * 2;
            alist.root = [11, 22, 33];

            const actual = alist.map(data);

            const expected = [22, 44, 66];
            assert.deepEqual(actual, expected);
        });

        it('should return [121, 484, 1089] if an argument is ((item) => item * item) and Alist.root is [11, 22, 33]', function() {
            const alist = new Alist(),
                data = (item) => item * item;
            alist.root = [11, 22, 33];

            const actual = alist.map(data);

            const expected = [121, 484, 1089];
            assert.deepEqual(actual, expected);
        });
    });

    describe('sort', function() {
        it('should return [] if Alist.root is []', function() {
            const alist = new Alist();

            const actual = alist.sort();

            const expected = [];
            assert.deepEqual(actual, expected);
        });

        it('should return [11] if Alist.root is [11]', function() {
            const alist = new Alist();
            alist.root = [11];

            const actual = alist.sort();

            const expected = [11];
            assert.deepEqual(actual, expected);
        });

        it('should return [11, 22] if Alist.root is [22, 11]', function() {
            const alist = new Alist();
            alist.root = [22, 11];

            const actual = alist.sort();

            const expected = [11, 22];
            assert.deepEqual(actual, expected);
        });

        it('should return [11, 22, 33] if Alist.root is [22, 11, 33]', function() {
            const alist = new Alist();
            alist.root = [22, 11, 33];

            const actual = alist.sort();

            const expected = [11, 22, 33];
            assert.deepEqual(actual, expected);
        });

        it('should return [33, 22, 11] if an argument is ((a, b) => a - b) and if Alist.root is [22, 11, 33]', function() {
            const alist = new Alist(),
                data = (a, b) => a - b;
            alist.root = [22, 11, 33];

            const actual = alist.sort(data);

            const expected = [33, 22, 11];
            assert.deepEqual(actual, expected);
        });
    });
})

// LINKED LIST
describe('Llist', function() {
    describe('createNode', function() {
        it('should return {value: undefined, next: null} if the first argument is empty', function() {
            const data = undefined,
                llist = new Llist();
    
            const actual = llist.createNode(data);
    
            const expected = {value: undefined, next: null};
            assert.deepEqual(actual, expected);
        });
    
        it('should return {value: undefined, next: null} if the second argument is empty', function() {
            const data1 = undefined,
                data2 = undefined,
                llist = new Llist();
    
            const actual = llist.createNode(data1, data2);
    
            const expected = {value: undefined, next: null};
            assert.deepEqual(actual, expected);
        });
    
        it('should return {value: 1, next: null} if the first argument is 1', function() {
            const data1 = 1,
                data2 = undefined,
                llist = new Llist();
    
            const actual = llist.createNode(data1, data2);
    
            const expected = {value: 1, next: null};
            assert.deepEqual(actual, expected);
        });
    
        it('should return {value: 1, next: {value: 2, next: null}} if the second argument is {value: 2, next: null}', function() {
            const data1 = 1,
                data2 = {value: 2, next: null},
                llist = new Llist();
    
            const actual = llist.createNode(data1, data2);
    
            const expected = {value: 1, next: {value: 2, next: null}};
            assert.deepEqual(actual, expected);
        });
    });

    describe('_getLastNodes', function () {
        it('should return [null, null] is an argument is empty', function() {
            const llist = new Llist();

            const actual = llist._getLastNodes();

            const expected = [null, null];
            assert.deepEqual(actual, expected);
        });

        it('should return [{value: 11, next: null}, {value: 11, next: null}] is an argument is {value: 11, next: null}', function() {
            const data = {value: 11, next: null},
                llist = new Llist();

            const actual = llist._getLastNodes(data);

            const expected = [{value: 11, next: null}, {value: 11, next: null}];
            assert.deepEqual(actual, expected);
        });

        it('should return [{value: 11, next: {value: 22, next: null}}, {value: 22, next: null}] is an argument is {value: 11, next: {value: 22, next: null}}', function() {
            const data = {value: 11, next: {value: 22, next: null}},
                llist = new Llist();

            const actual = llist._getLastNodes(data);

            const expected = [{value: 11, next: {value: 22, next: null}}, {value: 22, next: null}];
            assert.deepEqual(actual, expected);
        });
    });

    describe('_getLength', function() {
        it('should return 0 if an argument is null', function() {
            const data = null,
                llist = new Llist();

            const actual = llist._getLength(data);

            const expected = 0;
            assert.equal(actual, expected);
        });

        it('should return 1 if an argument is {value: 1, next: null}', function() {
            const data = {value: 1, next: null},
                llist = new Llist();

            const actual = llist._getLength(data);

            const expected = 1;
            assert.equal(actual, expected);
        });

        it('should return 3 if an argument is {value: 1, next: {value: 2, next: {value: 3, next: null}}}', function() {
            const data = {value: 1, next: {value: 2, next: {value: 3, next: null}}},
                llist = new Llist();

            const actual = llist._getLength(data);

            const expected = 3;
            assert.equal(actual, expected);
        });
    });

    describe('push', function() {
        it('should return 0 if an argument is empty and Llist.root is empty', function() {
            const llist = new Llist();
            
            const actual = llist.push();

            const expected = 0;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if an argument is empty and Llist.root is empty', function() {
            const llist = new Llist();
            
            llist.push();
            const actual = llist.root;

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 1 if an argument is 11', function() {
            const data = 11, 
                llist = new Llist();
                
            
            const actual = llist.push(data);

            const expected = 1;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal {value: 11, next: null} if an argument is 11 and Llist.root is empty', function() {
            const data = 11,
                llist = new Llist();
            
            llist.push(data);
            const actual = llist.root;

            const expected = {value: 11, next: null};
            assert.deepEqual(actual, expected);
        });

        it('should return 2 if argument are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            const actual = llist.push(data1, data2);

            const expected = 2;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal {value: 11, next: {value: 22, next: null}} if arguments are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            llist.push(data1, data2);
            const actual = llist.root;

            const expected = {value: 11, next: {value: 22, next: null}};
            assert.deepEqual(actual, expected);
        });

        it('should return 2 if argument are (22) and Llist.root is {value: 11, next: null}', function() {
            const data = 22,
                llist = new Llist();
            llist.root = {value: 11, next: null};

            const actual = llist.push(data);

            const expected = 2;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal {value: 11, next: {value: 22, next: null}} if arguments are (22) and Llist.root is {value: 11, next: null}', function() {
            const data = 22,
                llist = new Llist();
            llist.root = {value: 11, next: null};
            
            llist.push(data);
            const actual = llist.root;

            const expected = {value: 11, next: {value: 22, next: null}};
            assert.deepEqual(actual, expected);
        });
    });

    describe('pop', function() {
        it('should return undefined if Llist.root is empty', function() {
            const llist = new Llist();
    
            const actual = llist.pop();
    
            const expected = undefined;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root is empty', function() {
            const llist = new Llist();
    
            llist.pop();
            const actual = llist.root;
    
            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 11 if Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: null};
    
            const actual = llist.pop();
    
            const expected = 11;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: null};
    
            llist.pop();
            const actual = llist.root;
    
            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 33 if List.root is {value: 11, next: {value: 22, next: {value: 33, next: null}}}', function() {
            const llist = new Llist();
            llist.root = data = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
    
            const actual = llist.pop();
    
            const expected = 33;
            assert.equal(actual, expected);          
        });

        it('Llist.root should equal {value: 11, next: {value: 22, next: null}} if Llist root is {value: 11, next: {value: 22, next: {value: 33, next: null}}}', function() {
            const llist = new Llist();
            llist.root = data = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
            
            llist.pop();
            const actual = llist.root;

            const expected = {value: 11, next: {value: 22, next: null}}
            assert.deepEqual(actual, expected);
        });
    });

    describe('unshift', function() {
        it('should return 0 if an argument is empty and Llist.root is empty', function() {
            const llist = new Llist();
            
            const actual = llist.unshift();

            const expected = 0;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if an argument is empty and Llist.root is empty', function() {
            const llist = new Llist();
            
            llist.unshift();
            const actual = llist.root;

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 1 if an argument is 11', function() {
            const data = 11, 
                llist = new Llist();
            
            const actual = llist.unshift(data);

            const expected = 1;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal {value: 11, next: null} if an argument is 11 and Llist.root is empty', function() {
            const data = 11,
                llist = new Llist();
            
            llist.unshift(data);
            const actual = llist.root;

            const expected = {value: 11, next: null};
            assert.deepEqual(actual, expected);
        });

        it('should return 2 if argument are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            const actual = llist.unshift(data1, data2);

            const expected = 2;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal {value: 11, next: {value: 22, next: null}} if arguments are (11, 22) and Llist.root is empty', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            
            llist.unshift(data1, data2);
            const actual = llist.root;

            const expected = {value: 11, next: {value: 22, next: null}};
            assert.deepEqual(actual, expected);
        });

        it('should return 3 if argument are (11, 22) and Llist.root is {value: 33, next: null}', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            llist.root = {value: 33, next: null};
            
            const actual = llist.unshift(data1, data2);

            const expected = 3;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal {value: 11, next: {value: 22, next: {value: 33, next: null}}} if arguments are (11, 22) and Llist.root is {value: 33, next: null}', function() {
            const data1 = 11,
                data2 = 22,
                llist = new Llist();
            llist.root = {value: 33, next: null};
            
            llist.unshift(data1, data2);
            const actual = llist.root;

            const expected = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
            assert.deepEqual(actual, expected);
        });
    });

    describe('shift', function() {
        it('should return undefined if Llist.root is empty', function() {
            const llist = new Llist();

            const actual = llist.shift();

            const expected = undefined;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root is empty', function() {
            const llist = new Llist();

            llist.shift();
            const actual = llist.root;

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 11 if Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: null};
    
            const actual = llist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);
        });

        it('Llist.root should equal null if Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: null};
    
            llist.shift();
            const actual = llist.root;
    
            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return 11 if List.root is {value: 11, next: {value: 22, next: {value: 33, next: null}}}', function() {
            const llist = new Llist();
            llist.root = data = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
    
            const actual = llist.shift();
    
            const expected = 11;
            assert.equal(actual, expected);          
        });

        it('Llist.root should equal {value: 22, next: {value: 33, next: null}} if Llist root is {value: 11, next: {value: 22, next: {value: 33, next: null}}}', function() {
            const llist = new Llist();
            llist.root = data = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
            
            llist.shift();
            const actual = llist.root;

            const expected = {value: 22, next: {value: 33, next: null}}
            assert.deepEqual(actual, expected);
        });
    });

    describe('toString', function() {
        it('should return an empty string if Llist.root is epmty', function() {
            const llist = new Llist();
    
            const actual = llist.toString();
    
            const expected = '';
            assert.equal(actual, expected);
        });
    
        it('should return "11" if Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: null};
    
            const actual = llist.toString();
    
            const expected = '11';
            assert.equal(actual, expected);
        });
    
        it('should return "11, 22, 33" if Llist.root is {value: 11, next: {value: 22, next: {value: 33, next: null}}}', function() {
            const llist = new Llist();
            llist.root = {value: 11, next: {value: 22, next: {value: 33, next: null}}};
    
            const actual = llist.toString();
    
            const expected = '11, 22, 33';
            assert.equal(actual, expected);
        });
    });

    describe('map', function() {
        it('should return undefined if an argument is not a function', function() {
            const llist = new Llist()
                data = 'invalid data';

            const actual = llist.map(data);

            const expected = undefined;
            assert.equal(actual, expected);
        });

        it('should return null if an argument is ((item) => item * 2) and Llist.root is null', function() {
            const llist = new Llist(),
                data = (item) => item * 2;
            
            const actual = llist.map(data);

            const expected = null;
            assert.equal(actual, expected);
        });

        it('should return {value: 22, next: null} if an argument is ((item) => item * 2) and Llist.root is {value: 11, next: null}', function() {
            const llist = new Llist(),
                data = (item) => item * 2;
            llist.root = {value: 11, next: null};
            
            const actual = llist.map(data);

            const expected = {value: 22, next: null};
            assert.deepEqual(actual, expected);
        });

        it('should return {value: 121, next: {value: 484, next: null}} if an argument is ((item) => item * item) and Llist.root is {value: 11, next: {value: 22, next: null}}', function() {
            const llist = new Llist(),
                data = (item) => item * item;
            llist.root = {value: 11, next: {value: 22, next: null}};
            
            const actual = llist.map(data);

            const expected = {value: 121, next: {value: 484, next: null}};
            assert.deepEqual(actual, expected);
        });
    });
});

mocha.run();