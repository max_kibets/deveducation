class Alist {
    root = [];

    push = (...rest) => {
        const arr = this.root;

        for (const item of rest) {
            arr[arr.length] = item;
        };

        return arr.length;
    };

    pop = () => {
        let lastItem;

        if(this.root.length) {
            lastItem = this.root[this.root.length - 1];
            this.root.length = this.root.length - 1;
        }

        return lastItem;
    };

    unshift = (...rest) => {
        const arr = this.root;

        for (let i = rest.length - 1; i >= 0; i--) {
            arr.length = arr.length + 1;

            for (let i = arr.length - 1; i > 0; i--) {
                arr[i] = arr[i - 1];
            }

            arr[0] = rest[i];
        };

        return arr.length;
    };

    shift = () => {
        const arr = this.root;
        let firstItem;

        if(arr.length) {
            firstItem = arr[0];

            for (let i = 0; i < arr.length; i++) {
                arr[i] = arr[i + 1];
            }

            arr.length = arr.length - 1;
        }

        return firstItem;
    };

    toString = () => {
        const arr = this.root;
        let str = '';

        for (let i = 0; i < arr.length; i++) {
            if (i) {
                str += `, ${arr[i]}`;
            } else {
                str += arr[i]
            }
        }

        return str;
    };

    map = (callback) => {
        if (typeof callback !== 'function') return;
        
        let newArr = [];

        for (let i = 0; i < this.root.length; i++) {
            newArr[i] = callback(this.root[i], i, this.root);
        };

        return newArr;
    };

    _compare = (a, b) => {
        if (a > b) return 1;
        if (a < b) return -1;
        return 0;
    };

    sort = (comparator = this._compare) => {
        const sortedArray = this.root;

        function sortHalfs(start, end) {
            if (end - start < 1) return;

            const pivotValue = sortedArray[end];
            let splitIndex = start;

            for (let i = start; i < end; i++) {
                const sort = comparator(sortedArray[i], pivotValue);
          
                if (sort === -1) {
                    if (splitIndex !== i) {
                        const temp = sortedArray[splitIndex];

                        sortedArray[splitIndex] = sortedArray[i];
                        sortedArray[i] = temp;
                    };

                    splitIndex++;
                };
            };

            sortedArray[end] = sortedArray[splitIndex];
            sortedArray[splitIndex] = pivotValue;

            sortHalfs(start, splitIndex - 1);
            sortHalfs(splitIndex + 1, end);
        };

        sortHalfs(0, this.root.length - 1);
        return sortedArray;
    }
};

class Llist {
    root = null;

    createNode = (value, next = null) => {
        return {
            value,
            next
        }
    };

    _getLength = (root) => {
        if (!root) return 0;

        let count = 1;

        (function countItems(node) {
            if (node.next) {
                count++;
                countItems(node.next);
            };
        })(root);

        return count;
    };

    _getLastNodes = (root) => {
        if (!root) return [null, null];
        
        let prev = root,
            last = root.next;
        
        (function findNodes(node) {
            if (node.next) {
                prev = node;
                findNodes(node.next);
            } else {
                last = node;
            }
        })(root);

        return [
            prev,
            last
        ];
    };

    push = (...rest) => {
        if (!this.root && rest.length) {
            this.root = this.createNode(rest[0]);
            
            rest = rest.slice(1);
        };

        let last = this._getLastNodes(this.root)[1];

        for (let item of rest) {
            last.next = this.createNode(item);
            last = last.next;
        };

        return this._getLength(this.root);
    };

    pop = () => {
        if (!this.root) return;

        let value = this.root.value;

        if (this._getLength(this.root) === 1) {
            this.root = null;
        } else {
            value = this._getLastNodes(this.root)[1].value;
            this._getLastNodes(this.root)[0].next = null;
        };

        return value;
    };

    unshift = (...rest) => {
        if (!this.root && rest.length) {
            this.root = this.createNode(rest[rest.length - 1]);
            
            rest = rest.slice(0, -1);
        };

        for (let i = rest.length - 1; i >= 0; i--) {
            let first = this.createNode(rest[i]);
            
            first.next = this.root;
            this.root = first;
        }

        return this._getLength(this.root);
    };

    shift = () => {
        if (!this.root) return;

        const value = this.root.value;

        this.root = this.root.next;

        return value;
    };
    
    toString = () => {
        if (!this.root) return '';
        let str = '';

        (function concatValues(node) {
            if (node.next) {
                str += `${node.value}, `;
                concatValues(node.next);
            } else {
                str += node.value;
            };
        })(this.root);

        return str;
    };

    map = (callback) => {
        if (typeof callback !== 'function') return;

        let newRoot = null,
            root = this.root;

        if (!root) return newRoot;

        let rootLast = root;

        do {
            const value = callback(rootLast.value);

            if (newRoot) {
                const last = this._getLastNodes(newRoot)[1];

                last.next = this.createNode(value);
            } else {
                newRoot = this.createNode(value);
            };

            rootLast = rootLast.next;
        } while (rootLast);

        return newRoot;
    }
}

let llist = new Llist();
llist.push(11,22,33);
console.log(llist.map((item) => {return item * item}));
